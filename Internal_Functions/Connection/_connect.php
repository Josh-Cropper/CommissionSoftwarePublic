<?php
session_start();

class Connection
{
    protected static $_instance;

    protected $connection;
    protected $pdo;

    public static final function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static final function _i()
    {
        return self::getInstance();
    }

    public function __construct()
    {
        $this->connection = mysqli_connect(
            \Settings::_i()->getDBMSHost(),
            \Settings::_i()->getDBMSUsername(),
            \Settings::_i()->getDBMSPassword(),
            \Settings::_i()->getDBMSDatabase(),
            \Settings::_i()->getDBMSPort())
        or die(print "Error: Unable to connect to database:" . PHP_EOL);

        $this->pdo = new PDO(
            'mysql:host=' . \Settings::_i()->getDBMSHost() . ';port=' . \Settings::_i()->getDBMSPort() . ';dbname=' . \Settings::_i()->getDBMSDatabase(),
            \Settings::_i()->getDBMSUsername(),
            \Settings::_i()->getDBMSPassword()
        );
    }

    public function __destruct()
    {
        $this->connection->close();
        $this->pdo = null;
    }

    public function __get($name)
    {
        if (!in_array($name, ['connection', 'pdo'])) {
            return false;
        }
        if ($name == 'connection') {
            if (!$this->connection->stat()) {
                $this->connection->close();
                $this->connection = mysqli_connect(
                    \Settings::_i()->getDBMSHost(),
                    \Settings::_i()->getDBMSUsername(),
                    \Settings::_i()->getDBMSPassword(),
                    \Settings::_i()->getDBMSDatabase(),
                    \Settings::_i()->getDBMSPort())
                or die(print "Error: Unable to connect to database:" . PHP_EOL);
            }
            return $this->connection;
        } else {

            if (!$this->pdo->query('SELECT 1')) {
                $this->pdo = null;
                $this->pdo = new PDO(
                    'mysql:host=' . \Settings::_i()->getDBMSHost() . ';port=' . \Settings::_i()->getDBMSPort() . ';dbname=' . \Settings::_i()->getDBMSDatabase(),
                    \Settings::_i()->getDBMSUsername(),
                    \Settings::_i()->getDBMSPassword()
                );
            }
            return $this->pdo;
        }
    }
}