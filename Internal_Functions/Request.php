<?php

include_once 'Includes.php';

if(isset($_REQUEST['requestType']) && !empty($_REQUEST['requestType'])) {
    $requestHandler =
        preg_replace("/(\_)/","/",
            preg_replace("/(\/)/","",
                preg_replace("/(\.)/","", $_REQUEST['requestType'])));

    if(file_exists("Requests/".$requestHandler.".php")) {
        include_once("Requests/" . $requestHandler . ".php");
    } else {
        header("HTTP/1.0 404 Not Found");
    }
} //TODO: Start switching things to use this request handler and split requests into different files structured accordingly.