<?php
namespace Additional_Functions {

    function checkTwitch()
    {
        $minutes = 1;
        $twitch = "Twitch";
        if (!($command = \Database\getConnection()
            ->prepare("SELECT value, timestamp FROM System WHERE name = ?"))) {
            print "Preparation Failed";
        }

        if (!($command->bind_param('s', $twitch))) {
            print "Binding failed";
        }

        if (!($command->execute())) {
            print "Execution Failed";
        }

        if (!($command->store_result())) {
            print "Storage Failed";
        }

        if ($command->num_rows === 0) {
            $command->close();

            if (!($command = \Database\getConnection()
                ->prepare("INSERT INTO System (name, value, timestamp) VALUES (?,0,NOW())"))) {
                print "Preparation Failed";
            }

            if (!($command->bind_param("s", $twitch))) {
                print "Binding failed";
            }

            $command->execute();
            return true;
        } else {
            $command->bind_result($value, $timestamp);
            $command->fetch();

            if (strtotime($timestamp) <= (time() - (60 * $minutes))) {
                $command->close();
                return true;
            }
            return false;
        }
    }

    function updateTwitch($value)
    {
        if (!($command = \Database\getConnection()
            ->prepare("UPDATE System SET value = ?, timestamp = NOW() WHERE name = 'Twitch'"))) {
            print "Preparation Failed";
        }
        $bool = +$value;

        if (!($command->bind_param("i", $bool))) {
            print "Binding Failed";
        }

        if (!($command->execute())) {
            print "Execution Failed";
        }
    }

    function getTwitch()
    {
        if (!($command = \Database\getConnection()
            ->prepare("SELECT value FROM System WHERE name = 'Twitch'"))) {
            print "Preparation Failed";
        }

        if (!($command->execute())) {
            print "Execution Failed";
        }

        if (!($command->store_result())) {
            print "Storage Failed";
        }

        if (!($command->bind_result($value))) {
            print "Binding Failed";
        }

        $command->fetch();
        return $value;
    }

    function getCommissionTypes(): array
    {
        if (!($command = \Database\getConnection()
            ->prepare("SELECT ID, name, price, max_num FROM Commission_Types"))) {
            return array(
                'success' => false,
                'result' => "MySQLi Preparation Failed"
            );
        }

        if (!($command->execute())) {
            return array(
                'success' => false,
                'result' => "MySQLi Exectution Failed"
            );
        }

        if (!($command->store_result())) {
            return array(
                'success' => false,
                'result' => "MySQLi Result Storage Failed"
            );
        }

        if (!($command->bind_result($ID, $name, $price, $limit))) {
            return array(
                'success' => false,
                'result' => "MySQLi Result Binding Failed"
            );
        }

        $result = array(
            'success' => true,
            'result' => array());

        while ($command->fetch()) {
            $result['result'][$ID] = array(
                'ID' => $ID,
                'Name' => $name,
                'Price' => $price,
                'Limit' => $limit
            );
        }
        $command->close();
        return $result;
    }
}
