<?php declare (strict_types=1);

namespace Database {

    function getConnection()
    { //TODO: Gradually get rid of mysqli
        return \Connection::_i()->connection;
    }

    function getPDOConnection()
    {
        return \Connection::_i()->pdo;
    }

    function login(string $username, string $passwordHash): array
    {
        $usersTable = \Settings::_i()->getLoginTable();
        $usernameField = \Settings::_i()->getLoginUsernameColumn();
        $passwordField = \Settings::_i()->getLoginPasswordColumn();

        $sql = getPDOConnection()->prepare("SELECT ID, email, admin, avatar FROM {$usersTable} WHERE {$usernameField} = ? AND {$passwordField} = ?");
        $sql->execute([$username, $passwordHash]);
        $loginCheck = ($sql->rowCount() > 0);

        if (!$loginCheck) {
            return array(false, "login", "Incorrect Username or Password!");
        }

        $user = $sql->fetch(\PDO::FETCH_ASSOC);

        session_regenerate_id();
        $sessionID = session_id();

        $sql = getPDOConnection()->prepare("UPDATE {$usersTable} SET last_session_id = ? WHERE ID = ?");
        $sql->execute([$sessionID, $user['ID']]);

        return array(true, array(
            'ID' => $user['ID'],
            'Username' => $username,
            'Email' => $user['email'],
            'Rank' => $user['admin'],
            'Avatar' => $user['avatar'],
        ));
    }

    function log(string $ip = '', int $userID = 0, string $type, string $message): bool
    {
        return (bool)getConnection()->query("INSERT INTO Log (`IP`,`user_ID`,`key`,`value`) VALUES ('{$ip}',{$userID},'{$type}','{$message}')");
    }
}

namespace Database\Tables {

    function getTable(string $tablename)
    {
        if ($tablename == "") {
            throw new \InvalidArgumentException;
        } else {
            foreach (\Settings::_i()->DBMS->tables->table as $table) {
                if ($table->attributes()['ref'] == $tablename) {
                    return $table;
                }
            }
        }
    }

    function getTables(): \SimpleXMLElement
    {
        return \Settings::_i()->getSettings()->DBMS->tables;
    }
}

namespace Database\Account_Creation {

    function create_Account(string $value, string $username, string $email): array
    {
        $sql = \Database\getConnection()
            ->prepare("SELECT ID FROM Users WHERE username = ?");
        $sql->bind_param('s', $username);
        $sql->execute();
        $sql->store_result();
        $usernameCheck = ($sql->num_rows > 0);
        if (!$usernameCheck) {
            $sql = \Database\getConnection()
                ->prepare("SELECT ID FROM Users WHERE email = ?");
            $sql->bind_param('s', $email);
            $sql->execute();
            $sql->store_result();
            $emailCheck = ($sql->num_rows > 0);
            if (!$emailCheck) {
                if (!($sql = \Database\getConnection()->prepare("INSERT INTO Account_Create(token, username, email) VALUES (?,?,?)"))) {
                    return array(false, "Oops, Preparation Went Wrong!");
                }
                $sql->bind_param('sss', $value, $username, $email);
                if (!$sql->execute()) {
                    return array(false, "Oops, Execution Went Wrong! $email");
                }
                return array(true);
            } else {
                return array(false, "Email Taken!");
            }
        } else {
            return array(false, "Username Taken!");
        }
    }

    function check_Email(string $email): bool
    {
        $sql = \Database\getConnection()
            ->prepare("SELECT date FROM Account_Create WHERE email = ?");
        $sql->bind_param('s', $email);
        $sql->execute();
        return (bool)$sql->store_result()->num_rows;
    }

    function checkValue(string $value): bool
    {
        $sql = \Database\getConnection()
            ->prepare("SELECT date FROM Account_Create WHERE token = ?");
        $sql->bind_param("s", $value);
        $sql->execute();
        return (bool)$sql->store_result()->num_rows;
    }
}

namespace Database\Account_Edit {

    function changeAvatar(string $image)
    {
        if ($image == NULL) {
            print "No Image";
            return;
        }

        $userID = $_SESSION['User']['ID'];

        $sql = \Database\getConnection()
            ->prepare("UPDATE Users SET avatar = ? WHERE ID = ?");

        if (!($sql->bind_param("si", $image, $userID))) {
            print("Binding parameters failed: ($sql->errno) $sql->error");
        }

        if (!($sql->execute())) {
            $_SESSION['Error']['Type'] = "avatar_upload";
            $_SESSION['Error']['Message'] = "Avatar upload failed!";
        } else {
            $_SESSION['User']['Avatar'] = $image;
        }

        $sql->close();
        header("Location: " . \Settings::_i()->getRoot() . "/My_Account");
    }

    function passwordResetEmail(string $username): bool
    {
        $type = "password-reset";

        $user = \Database\Account\getUserByUsername($username);
        if ($user === false) {
            return false;
        }

        $rowCheck = \Database\getConnection()->prepare("SELECT * FROM Account_Edit WHERE user_ID = ? AND type = 'password-reset'");
        $rowCheck->bind_param("i", $user['ID']);
        $rowCheck->execute();
        $rowCheck = $rowCheck->get_result()->num_rows;

        if ($rowCheck) {
            return false;
        }

        $value = \Functions\randomString(6);
        while (\Database\Account_Creation\checkValue($value)) {
            $value = \Functions\randomString(6);
        }

        $sql = \Database\getConnection()->prepare("INSERT INTO Account_Edit (token, type, user_ID) VALUES (?,?,?)");

        if (!$sql->bind_param("ssi", $value, $type, $user['ID'])) {
            return false;
        }

        if (!$sql->execute()) {
            return false;
        }

        \Functions\sendEmail($user['email'], "OvOcommissions Password Reset",
            str_replace("\$token", $value, \Settings::_i()->emails->passwordReset));

        return true;
    }

    function checkValue(string $value, string $type): bool
    {
        $sql = \Database\getConnection()
            ->prepare("SELECT user_ID FROM Account_Edit WHERE token = ? AND type = ?");
        $sql->bind_param("ss", $value, $type);
        $sql->execute();
        return (bool)$sql->store_result()->num_rows;
    }

}

namespace Database\Account {

    function getUsers(int $userID = 0, int $limit = 50, int $offset = 0, bool $forHTML = false)
    {
        if ($userID == 0) {
            $sql = \Database\getPDOConnection()->query("SELECT ID, username, email, admin as rank, avatar FROM Users LIMIT {$offset}, {$limit}");
        } else {
            $sql = \Database\getPDOConnection()->prepare("SELECT ID, username, email, admin as rank, avatar FROM Users WHERE ID = ? LIMIT {$offset}, {$limit}");
            $sql->execute([$userID]);
        }

        $users = $sql->fetchAll(\PDO::FETCH_ASSOC);

        if ($forHTML) {
            foreach ($users as $key => $user) {
                if ($user['avatar'] == null) {
                    $users[$key]['avatar'] = "<span class='fa fa-user' style='font-size: 30px;'></span>";
                } else {
                    $users[$key]['avatar'] = "<img src=\"data:image/png;base64," .
                        \Image_Handler::_i()->showImage($user['avatar'], 34, 34, false) . "\" />";
                }

                if ($user['rank'] == 999) {
                    $users[$key]['rank'] = "<span class='fa fa-check' style='color: forestgreen; text-shadow: 2px 2px 2px rgba(0,0,0,0.2);'></span>";
                } else {
                    $users[$key]['rank'] = "<span class='fa fa-close' style='color: darkred; text-shadow: 2px 2px 2px rgba(0,0,0,0.2);'></span>";
                }
            }
        }
        return $users;
    }

    function getUserByUsername(string $username)
    {
        $user = \Database\getPDOConnection()->prepare("SELECT * FROM Users WHERE `username` = :_username LIMIT 1");
        $user->bindParam(':_username', $username);
        $user->execute();
        $user = $user->fetchAll(\PDO::FETCH_ASSOC);

        if (!count($user)) {
            return false;
        }

        return $user[0];
    }

    function getUserBySessID(string $sessionID)
    {
        $user = \Database\getPDOConnection()->prepare("SELECT Users.* FROM Users WHERE `last_session_id` = ? LIMIT 1");
        $user->execute([$sessionID]);
        $user = $user->fetchAll(\PDO::FETCH_ASSOC);

        if (!count($user)) {
            return false;
        }

        return $user[0];
    }
}

namespace Database\Images {

    function fetchGalleryImages(int $page = 1, bool $imageData = true, int $pageSize = 20): array
    {
        $selectImageData = $imageData ? ", Images.data" : "";
        $offset = ($page - 1) * $pageSize;
        $limit = $page * $pageSize;

        $command = \Database\getConnection()
            ->prepare("
                SELECT Images.ID, Images.name, Images.version, Users.ID, Users.username{$selectImageData} FROM Images
                LEFT JOIN Users ON Images.user_ID = Users.ID
                LEFT JOIN Commissions ON Images.commission_ID = Commissions.ID
                WHERE Images.hidden = 0 AND (Commissions.finalised = 1 OR Images.commission_ID = 0) AND Images.version =
                (SELECT i.version FROM Images i WHERE Images.ID = i.ID ORDER BY version desc LIMIT 1)
                ORDER BY Images.ID desc
                LIMIT {$offset}, {$limit}"
            );
        $command->execute();
        $command->store_result();
        if ($imageData) {
            $command->bind_result($resID, $name, $version, $userID, $resUsername, $data);
        } else {
            $command->bind_result($resID, $name, $version, $userID, $resUsername);
        }

        $images = array();

        while ($command->fetch()) {
            if ($imageData) {
                array_push($images, array(
                    'ID' => $resID,
                    'name' => $name,
                    'data' => $data,
                    'version' => $version,
                    'userID' => $userID,
                    'username' => $resUsername
                ));
            } else {
                array_push($images, array(
                    'ID' => $resID,
                    'name' => $name,
                    'version' => $version,
                    'userID' => $userID,
                    'username' => $resUsername
                ));
            }
        }
        $command->close();

        return $images;
    }

    function fetchImages(array $IDs): array
    {
        foreach ($IDs as $ID) {
            $imageIDs = implode(", ", $IDs);
        }

        $command = \Database\getPDOConnection()
            ->prepare("
                SELECT Images.ID, ANY_VALUE(Images.name) name, MAX(Images.version) version,
ANY_VALUE(Users.ID) user_ID, ANY_VALUE(Users.username) username, ANY_VALUE((SELECT i.`data` FROM Images i WHERE i.ID = Images.ID AND i.version = MAX(Images.version))) `data`
FROM Images
LEFT JOIN Users ON Images.user_ID = Users.ID
LEFT JOIN Commissions ON Images.commission_ID = Commissions.ID
WHERE Images.ID IN ({$imageIDs})
GROUP BY Images.ID
ORDER BY Images.ID DESC"
            );
        if (!$command->execute()) {
            return array(false, $command->errorCode());
        }

        return $command->fetchAll(\PDO::FETCH_ASSOC);
    }

    function fetch_commission_images(int $commissionID, bool $limitVersion = false, int $ID = 0, bool $base64 = false, int $height = 0, int $width = 0): array
    {
        if ($limitVersion) {
            $limitVersion = " AND Images.version =
                (SELECT MAX(version) FROM Images i WHERE i.ID = Images.ID LIMIT 1)";
        }
        if ($ID > 0) {
            $ID = " AND Images.ID = {$ID}";
        } else {
            $ID = '';
        }

        $command = \Database\getPDOConnection()
            ->prepare("
                SELECT Images.ID, (SELECT i.name FROM Images i WHERE i.ID = Images.ID AND i.version = 1 LIMIT 1) name, Images.version, Images.data, Invoices.status
                FROM Images
                LEFT JOIN Commissions ON Images.commission_ID = Commissions.ID
                LEFT JOIN Invoices ON Commissions.invoice = Invoices.ID
                WHERE Images.commission_ID = ?{$ID}{$limitVersion} ORDER BY Images.name, Images.version desc;");
        $command->execute([$commissionID]);

        //echo $command->queryString;

        $images = $command->fetchAll(\PDO::FETCH_ASSOC);

        foreach($images as $key => $image) {
            if ($base64) {
                $images[$key]['data'] = \Image_Handler::_i()->showImage($image['data'], $height, $width, $image['status'] != 'Completed');
            }
        }

        return $images;
    }

}

namespace Database\Commissions {

    function add_commission(int $userID, int $type, int $numOf, string $descr): array
    {
        if (!($command = \Database\getConnection()
            ->prepare("INSERT INTO Commissions (user_ID, specification_text, type, amount) VALUES (?,?,?,?)"))) {
            return array(
                'success' => false,
                'result' => "MySQLi Preparation Failed"
            );
        }

        if (!($command->bind_param("isii", $userID, $descr, $type, $numOf))) {
            return array(
                'success' => false,
                'result' => "MySQLi Parametre Binding Failed"
            );
        }

        if (!($command->execute())) {
            return array(
                'success' => false,
                'result' => "MySQLi Exectution Failed"
            );
        }

        $command->close();
        return array(
            'success' => true,
            'result' => "Commission Added"
        );
    }

    function getCommissions(int $userID = 0, int $limit = 0, int $offset = 0, string $orderBy = null)
    {
        if (!($command = \Database\getPDOConnection()
            ->prepare("SELECT Commissions.*, Commission_Types.name, username FROM Commissions
                                 INNER JOIN Users ON Commissions.user_ID=Users.ID
                                 INNER JOIN Commission_Types ON Commissions.type=Commission_Types.ID" .
                (($userID == 0) ? "" : " WHERE user_ID = :_userID") .
                (($limit == 0) ? "" : " LIMIT :_limit") .
                (($offset == 0) ? "" : " OFFSET :_offset") .
                (($orderBy == null) ? "" : " ORDER BY {$orderBy}")))) {
            return array(
                'success' => false,
                'result' => "PDO Preparation Failed: " .
                    "SELECT Commissions.*, Commission_Types.name, username FROM Commissions
                                 INNER JOIN Users ON Commissions.user_ID=Users.ID
                                 INNER JOIN Commission_Types ON Commissions.type=Commission_Types.ID" .
                    (($userID == 0) ? "" : " WHERE user_ID = ?") .
                    (($limit == 0) ? "" : " LIMIT ?") .
                    (($offset == 0) ? "" : " OFFSET ?") .
                    (($orderBy == null) ? "" : " ORDER BY {$orderBy}")
            );
        }

        if ($userID != 0) {
            if (!($command->bindParam(
                ":_userID", $userID)
            )) {
                return array(
                    'success' => false,
                    'result' => "PDO Parametre Binding Failed"
                );
            }
        }

        if ($limit != 0) {
            if (!($command->bindParam(
                    ":_limit", $limit))
                || !($command->bindParam(
                    ":_offset", $offset))) {
                return array(
                    'success' => false,
                    'result' => "PDO Parametre Binding Failed"
                );
            }
        }

        if (!($command->execute())) {
            return array(
                'success' => false,
                'result' => "PDO Exectution Failed"
            );
        }

        $result = array(
            'success' => true,
            'result' => array());

        while ($row = $command->fetch()) {
            $result['result'][$row['ID']] = $row;
        }

        return $result;
    }

    function getCommission(int $commissionID): array
    {
        $command = \Database\getConnection()->prepare("SELECT Commissions.*, Commission_Types.name, Commission_Types.price, Invoices.status
                                                                            FROM Commissions
                                                                            LEFT JOIN Commission_Types ON Commissions.type=Commission_Types.ID
                                                                            LEFT JOIN Invoices ON Commissions.invoice = Invoices.ID
                                                                            WHERE Commissions.ID = ? LIMIT 1");
        $command->bind_param("i", $commissionID);
        $command->execute();
        $result = $command->get_result();
        $row = $result->fetch_assoc();
        return $row;
    }

    function commissionExists(int $ID): bool
    {
        if (!($command = \Database\getConnection()
            ->prepare("SELECT type FROM Commissions WHERE ID = ?"))) {
            return false;
        }

        if (!($command->bind_param("i", $ID))) {
            return false;
        }

        if (!($command->execute())) {
            return false;
        }

        if (!($command->store_result())) {
            return false;
        }

        return (bool)$command->num_rows;
    }

    function checkUserAccess(string $sessionOrUserID, int $commissionID): bool
    {
        $sql = \Database\getPDOConnection()->prepare("SELECT admin FROM Commissions
                                                         INNER JOIN Users ON user_ID=Users.ID OR Users.admin=999
                                                         WHERE ? IN (Users.ID,Users.last_session_id) AND Commissions.ID = ?
                                                         LIMIT 1");
        $sql->execute([$sessionOrUserID, $commissionID]);
        if ($sql->rowCount()) {
            return true;
        }

        return false;
    }
}

namespace Database\Security {

    function checkFailedLogins(string $IP, int $minutes = 0, int $hours = 0, int $days = 0, int $weeks = 0, int $months = 0, int $years = 0): int
    {
        if (!count(array_filter([$minutes, $hours, $days, $weeks, $months, $years]))) {
            return 0;
        }

        $time = new \DateInterval("P" .
            (($years) ? "{$years}Y" : "") .
            (($months) ? "{$months}M" : "") .
            (($weeks) ? $weeks * 7 . "D" : "") .
            (($days) ? "{$days}D" : "") .
            "T" .
            (($hours) ? "{$hours}H" : "") .
            (($minutes) ? "{$minutes}M" : "")
        );

        return (int)\Database\getConnection()->query("SELECT COUNT(*) FROM Log WHERE `IP` = '{$IP}' AND `key` = 'login' AND INSTR(`value`, \"\\\"success\\\":false\")
                                                  AND NOW() < ADDTIME(`timestamp`,'{$time->format('%H:%I:%S')}')")->fetch_array(MYSQLI_NUM)[0];
    }

    function addBan(int $userID, int $minutes = 0, int $hours = 0, int $days = 0, int $weeks = 0, int $months = 0, int $years = 0): bool
    {
        if (!count(array_filter([$minutes, $hours, $days, $weeks, $months, $years]))) {
            return false;
        }

        $time = new \DateInterval("P" .
            (($years) ? "{$years}Y" : "") .
            (($months) ? "{$months}M" : "") .
            (($weeks) ? $weeks * 7 . "D" : "") .
            (($days) ? "{$days}D" : "") .
            "T" .
            (($hours) ? "{$hours}H" : "") .
            (($minutes) ? "{$minutes}M" : "")
        );

        return (bool)\Database\getConnection()->query("INSERT INTO Bans (`type`,`user_ID`,`length`) VALUES ('user',{$userID},'{$time->format('%H:%I:%S')}')");
    }

    function addIPBan(string $IP, int $minutes = 0, int $hours = 0, int $days = 0, int $weeks = 0, int $months = 0, int $years = 0): bool
    {
        if (!count(array_filter([$minutes, $hours, $days, $weeks, $months, $years]))) {
            return false;
        }

        $time = new \DateInterval("P" .
            (($years) ? "{$years}Y" : "") .
            (($months) ? "{$months}M" : "") .
            (($weeks) ? $weeks * 7 . "D" : "") .
            (($days) ? "{$days}D" : "") .
            "T" .
            (($hours) ? "{$hours}H" : "") .
            (($minutes) ? "{$minutes}M" : "")
        );

        return (bool)\Database\getConnection()->query("INSERT INTO Bans (`IP`,`length`) VALUES ('{$IP}','{$time->format('%H:%I:%S')}')");
    }

    function checkIPBan(string $IP): bool
    {
        return (bool)\Database\getConnection()->query("SELECT COUNT(*) FROM Bans WHERE `type` = 'IP' AND `IP` = '{$IP}' AND NOW() < ADDTIME(`timestamp`, `length`)")->fetch_array(MYSQLI_NUM)[0];
    }

}