<?php
declare(strict_types=1);

class Settings
{

    protected static $_instance;

    private $settings;
    public $publicGIT = true; //Do not use unless you plan on forking this GIT as it will prevent settings caching
    private $tempSettingsTTL = 5;

    public static final function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static final function _i()
    {
        return self::getInstance();
    }

    public function __construct()
    {
        if (apcu_exists('mjolnir_settings_cache')) {
            $this->settings = simplexml_load_string(apcu_fetch('mjolnir_settings_cache'));
        } else {
            $this->settings = simplexml_load_file(__DIR__ . '/SETTINGS.xml');
            if ($this->publicGIT) {
                $hiddenSettings = simplexml_load_file(__DIR__ . '/HIDDEN-SETTINGS.xml');
                $this->settings->salt = $hiddenSettings->salt;
                $this->settings->DBMS->host = $hiddenSettings->host;
                $this->settings->DBMS->username = $hiddenSettings->username;
                $this->settings->DBMS->password = $hiddenSettings->password;
                $this->settings->DBMS->database = $hiddenSettings->database;
                $this->settings->DBMS->port = $hiddenSettings->port;
            }
            apcu_store('mjolnir_settings_cache', $this->settings->asXML(), $this->tempSettingsTTL*60);
        }
    }

    function __destruct()
    {
        if ($this->settings->asXML() != apcu_fetch('mjolnir_settings_cache')) {
            if (!is_dir(__DIR__ . '/backups/')) {
                mkdir(__DIR__ . '/backups/');
            }
            $i = 0;
            while (($file = readdir(opendir(__DIR__ . '/backups/'))) !== FALSE) {
                if (in_array($file, ['.', '..'])) {
                    break;
                }
                if ($i > 48) {
                    unlink(__DIR__ . "/backups/{$file}");
                }
                $i++;
            }

            copy(__DIR__ . '/SETTINGS.xml', __DIR__ . '/backups/SETTINGS-' . (new DateTime("now"))->format("Y-m-d-H-i-s") . '.xml');

            $file = fopen(__DIR__ . '/SETTINGS.xml', 'w');

            if ($this->publicGIT) {
                $this->settings->salt = '';
                $this->settings->DBMS->host = '';
                $this->settings->DBMS->username = '';
                $this->settings->DBMS->password = '';
                $this->settings->DBMS->database = '';
                $this->settings->DBMS->port = '';
            }

            fwrite($file, $this->settings->asXML());
            fclose($file);

            apcu_store('mjolnir_settings_cache', $this->settings->asXML(), $this->tempSettingsTTL*60);

        }
    }

    public function __get($name)
    {
        switch (strtolower($name)) {
            case 'dbms':
                return $this->settings->DBMS;
                break;
            case 'dbmshost':
                return stripslashes((string)$this->settings->DBMS->host);
                break;
            case 'dbmsusername':
                return stripslashes((string)$this->settings->DBMS->username);
                break;
            case 'dbmspassword':
                return stripslashes((string)$this->settings->DBMS->password);
                break;
            case 'dbmsdatabase':
                return stripslashes((string)$this->settings->DBMS->database);
                break;
            case 'navigation':
                return $this->settings->navigation;
                break;
            case 'salt':
                return $this->settings->salt;
                break;
            case 'paypal':
                return $this->settings->paypal->account;
                break;
            case 'emails':
                return $this->settings->emails;
                break;
            default:
                return null;
        }
    }

    public function __set($name, $value)
    {
        throw new Exception("Settings values can be changed with dedicated functions only, this is to prevent accidental changes to configuration files.");
    }

    public function getDBMSHost(): string
    {
        return stripslashes((string)$this->settings->DBMS->host);
    }

    public function getDBMSUsername(): string
    {
        return stripslashes((string)$this->settings->DBMS->username);
    }

    public function getDBMSPassword(): string
    {
        return stripslashes((string)$this->settings->DBMS->password);
    }

    public function getDBMSDatabase(): string
    {
        return stripslashes((string)$this->settings->DBMS->database);
    }

    public function getDBMSPort(): int
    {
        return (int)$this->settings->DBMS->port;
    }

    public function getName(): string
    {
        return (string)$this->settings->name;
    }

    public function getRoot(): string
    {
        return (string)$this->settings->root;
    }

    public function getLoginTable(): string
    {
        return (string)\Database\Tables\getTable(
            (string)$this->settings->DBMS->loginTable->ref
        );
    }

    public function getLoginUsernameColumn(): string
    {
        return (string)$this->settings->DBMS->loginTable->usernameField;
    }

    public function getLoginPasswordColumn(): string
    {
        return (string)$this->settings->DBMS->loginTable->passwordField;
    }
}