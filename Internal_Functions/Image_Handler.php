<?php declare(strict_types=1);

include_once "Includes.php";

//SELECT ID, MAX(`version`), (SELECT `data` FROM Images_New as IN2 WHERE IN2.ID = ID ORDER BY IN2.`version` desc LIMIT 1), name FROM Images_New GROUP BY ID;

class Image_Handler
{

    protected $watermark;
    protected static $_instance;

    public static final function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static final function _i()
    {
        return self::getInstance();
    }

    public function __construct()
    {
        if (extension_loaded("apcu")) {
            if (apcu_exists("cs_watermark")) {
                $this->watermark = apcu_fetch("cs_watermark");
            } else {
                $this->watermark = \Database\getConnection()->query("SELECT `valueBLOB` FROM System WHERE `name` = 'Watermark'")->fetch_assoc()['valueBLOB'];
                apcu_store("cs_watermark", $this->watermark);
                $this->watermark = $this->watermark;
            }
        } else {
            $this->watermark = \Database\getConnection()->query("SELECT `valueBLOB` FROM System WHERE `name` = 'Watermark'")->fetch_assoc()['valueBLOB'];
        }
    }

    public function insertImage(string $image, string $name): array
    {
        if ($image == "") {
            return [false, "No image data provided."];
        } else if ($name == "") {
            return array(false, "No image name provided.");
        }

        $command = \Database\getConnection()
            ->prepare("INSERT INTO Images (name, data) VALUES (?,?)");

        if (!($command->bind_param("ss", $name, $image))) {
            print("Binding parameters failed: ($command->errno) $command->error");
        }

        if (!($command->execute())) {
            print("Execute failed: ($command->errno) $command->error");
        }

        $command->close();
        return array(true, "Image inserted successfully.");
    }

    public function insertImageNew(string $image, int $ID, string $name): array
    {
        if ($image == "") {
            return [false, "No image data provided."];
        } else if ($name == "") {
            return array(false, "No image name provided.");
        }

        $inserts = [
            'data' => $image
        ];

        if (isset($ID) && $ID > 0) $inserts['ID'] = $ID;
        if (isset($name) && $name != '') $inserts['name'] = $name;

        $cols = "";
        $colsReplace = "";

        foreach ($inserts as $key => $value) {
            $cols .= "`$key`, ";
            $colsReplace .= ":_$key, ";
        }
        $cols = substr($cols, 0, strlen($cols)-2);
        $colsReplace = substr($colsReplace, 0, strlen($colsReplace)-2);

        $command = \Database\getPDOConnection()
            ->prepare("INSERT INTO Images_New ({$cols}) VALUES ({$colsReplace})");

        foreach ($inserts as $key => $value) {
            $command->bindParam(":_$key", $value);
        }

        if (!($command->execute())) {
            print("Execute failed: ({$command->errorCode()}) {$command->errorInfo()[2]}\n");
            print($command->queryString);
        }

        return array(true, "Image inserted successfully.");
    }

    public function getWatermark(int $width, int $height)
    {
        $wHeight = 0;
        if ($width < $height) {
            $wHeight = ($height * 0.66 > $width) ? $width : (int)round($height * 0.66);
        } else {
            $wHeight = ($width * 0.66 > $height) ? $height : (int)round($width * 0.66);
        }

        $image = imagecreatefromstring($this->watermark);

        $newImage = imagecreatetruecolor($wHeight, $wHeight);
        imagealphablending($newImage, false);
        imagesavealpha($newImage, true);
        $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
        imagefilledrectangle($newImage, 0, 0, $wHeight, $wHeight, $transparent);
        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $wHeight, $wHeight, imagesx($image), imagesy($image));

        imagedestroy($image);
        return $newImage;
    }

    public function parseImage(string $data, int $height = 0, int $width = 0, bool $watermark = true)
    {
        $image = imagecreatefromstring($data);
        imagesavealpha($image, true);
        if ($height==$width && $width == 0) {
            if ($watermark) {
                $watermark = $this->getWatermark(imagesx($image), imagesy($image));
                imagealphablending($image, true);
                imagesavealpha($watermark, true);
                imagealphablending($watermark, true);
                imagecopy($image, $watermark, 0, $height - imagesy($watermark), 0, 0, imagesx($watermark), imagesy($watermark));
            }
            return $image;
        } elseif ($height && !$width) {
            $width = (int)round((imagesx($image) / imagesy($image)) * $height);

            $newImage = imagecreatetruecolor($width, $height);
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $width, $height, $transparent);
            imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, imagesx($image), imagesy($image));
            if ($watermark) {
                $watermark = $this->getWatermark($width, $height);
                imagealphablending($newImage, true);
                imagesavealpha($watermark, true);
                imagealphablending($watermark, true);
                imagecopy($newImage, $watermark, 0, $height - imagesy($watermark), 0, 0, imagesx($watermark), imagesy($watermark));
            }
            imagedestroy($image);
            return $newImage;
        } elseif ($width > 0 && $height == 0) {
            $height = (int)round((imagesy($image) / imagesx($image)) * $width);

            $newImage = imagecreatetruecolor($width, $height);
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $width, $height, $transparent);
            imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, imagesx($image), imagesy($image));
            if ($watermark) {
                $watermark = $this->getWatermark($width, $height);
                imagealphablending($newImage, true);
                imagesavealpha($watermark, true);
                imagealphablending($watermark, true);
                imagecopy($newImage, $watermark, 0, $height - imagesy($watermark), 0, 0, imagesx($watermark), imagesy($watermark));
            }
            imagedestroy($image);
            return $newImage;
        } else {
            if (imagesx($image) > imagesy($image)) {
                $height = (int)round((imagesy($image) / imagesx($image)) * $width) <= $height
                    ? (int)round((imagesy($image) / imagesx($image)) * $width)
                    : $height;

                $height == (int)round((imagesy($image) / imagesx($image)) * $width)
                    ?: ($width = $width = (int)round((imagesx($image) / imagesy($image)) * $height));
            } else {
                $width = (int)round((imagesx($image) / imagesy($image)) * $height) <= $width
                    ? (int)round((imagesx($image) / imagesy($image)) * $height)
                    : $width;

                $width == (int)round((imagesx($image) / imagesy($image)) * $height)
                    ?: $height = (int)round((imagesy($image) / imagesx($image)) * $width);
            }

            $newImage = imagecreatetruecolor($width, $height);
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $width, $height, $transparent);
            imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, imagesx($image), imagesy($image));
            if ($watermark) {
                $watermark = $this->getWatermark($width, $height);
                imagealphablending($newImage, true);
                imagesavealpha($watermark, true);
                imagealphablending($watermark, true);
                imagecopy($newImage, $watermark, 0, $height - imagesy($watermark), 0, 0, imagesx($watermark), imagesy($watermark));
            }
            imagedestroy($image);
            return $newImage;
        }
    }

    public function showImage(string $data, int $height = 0, int $width = 0, bool $watermark = true): string
    {
        ob_start();
        imagepng($this->parseImage($data, $height, $width, $watermark));

        return base64_encode(ob_get_clean());
    }

    public function displayImage(int $ID, int $height = 0, int $width = 0, bool $watermark = true): string
    {
        if (extension_loaded("apcu")) {
            if (apcu_exists("csih_{$ID}_{$height}_{$width}_{$watermark}")) {
                return apcu_fetch("csih_{$ID}_{$height}_{$width}_{$watermark}");
            } else {
                $image = $this->showImage(\Database\Images\fetchImages([$ID])[0]['data'], $height, $width, $watermark);
                apcu_store("csih_{$ID}_{$height}_{$width}_{$watermark}", $image, 30 * 60);
                return $image;
            }
        }
        return $this->showImage(\Database\Images\fetchImages([$ID])[0]['data'], $height, $width, $watermark);
    }

    public function showWatermark(): string
    {
        ob_start();
        imagepng($this->getWatermark(100, 100));

        return base64_encode(ob_get_clean());
    }
}