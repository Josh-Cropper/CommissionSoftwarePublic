<?php

//include_once("../Includes.php");

class PageBuilder
{

    protected static $_instance;

    protected $jsIncludes;
    protected $cssIncludes;
    protected $phpIncludes;
    protected $userRank;

    protected $pages;

    public static final function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static final function _i() {
        return self::getInstance();
    }

    protected function __construct()
    {
        $this->jsIncludes = array();
        $this->cssIncludes = array(); //For eventual ajax pagebuilding
        $this->phpIncludes = array();

        $this->userRank = 0;

        $this->pages = simplexml_load_file(__DIR__ . '/PAGES.xml');
    }

    public function getUserRank(): int
    {
        return $this->userRank;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function getPage($pageTitle)
    {
        foreach ($this->getPages()->page as $page) {
            if ($page->title == $pageTitle) {
                return $page;
            }
        }
    }

    public function getMainPage()
    {
        foreach ($this->getPages()->page as $page) {
            if (isset($page->main)) {
                return (string) $page->title;
                break;
            }
        }
        return "";
    }

    public function isSubHandler($pageTitle)
    {
        if (in_array($pageTitle, $this->getPages()->xpath("/PAGES/page/subHandler/../title"))) {
            return true;
        } else {
            return false;
        }
    }

    public function pageExists($pageTitle)
    {
        if (in_array($pageTitle, $this->getPages()->xpath("/PAGES/page/title"))) {
            return true;
        } else {
            return false;
        }
    }

    public function getPageElements($page)
    {
        $elements = array();
        foreach ($page->element as $element) {
            array_push($elements, $element);
        }
        return $elements;
    }

    public function getPageStyles($page)
    {
        $elements = array();
        foreach ($page->style as $element) {
            array_push($elements, $element);
        }
        return $elements;
    }

    public function buildHeader($page)
    {
        include __DIR__ . '/Elements/_Core/Header.php';
    }

    public function buildFooter($page)
    {
        include __DIR__ . '/Elements/_Core/Footer.php';
    }

    public function sessionControl($page)
    {
        if(isset($page->sessionControl)) {
            include __DIR__ . '/Elements/_Core/Session_Control.php';
        }
    }

    public function buildNavbar($page)
    {
        include __DIR__ . "/Elements/_Navbars/$page->navbar.php";
    }

    public function buildContent($page)
    {
        $elements = $this->getPageElements($page);

        foreach ($elements as $element) {
            if (file_exists(__DIR__ . "/Elements/$element->name.js")) { // Needs re-implementing in a better way.
                print "<script type=\"text/javascript\" src=\"" . \Settings::_i()->getRoot() . "/Internal_Functions/PageBuilder/Elements/$element->name.js\"></script>";
            } elseif (file_exists(__DIR__ . "/Elements/$element->name.js.php")) { // Needs re-implementing in a better way.
                print "<script type=\"text/javascript\" src=\"" . \Settings::_i()->getRoot() . "/Internal_Functions/PageBuilder/Elements/$element->name.js.php\"></script>";
            }
            include __DIR__ . "/Elements/$element->name.php";
        }
    }

    public function buildElement(string $elementName, ...$args)
    {
        //echo var_dump($args);
        if (file_exists(__DIR__ . "/Elements/{$elementName}/{$elementName}.php")) {
            include __DIR__ . "/Elements/{$elementName}/{$elementName}.php";
            if (file_exists(__DIR__ . "/Elements/{$elementName}/main.js.php")) {
                print "<script type=\"text/javascript\" src=\"" . \Settings::_i()->getRoot() .
                    "/Internal_Functions/PageBuilder/Elements/{$elementName}/main.js.php\"></script>";
            }
        }
    }

    public function buildPage($pageTitle)
    {
        $page = $this->getPage($pageTitle);

        if (isset($_SESSION['User'])) {
            $this->userRank = $_SESSION['User']['Rank'];
        } else {

        }
        $pageRanks = $page->attributes()['ranks'];
        if (!Functions\checkAccess($pageRanks, $this->userRank)) {
            header("Location: " . \Settings::_i()->getRoot() .
                "/" . $this->getMainPage());
            return;
        }

        ob_start();

        $this->buildHeader($page);

        $this->buildNavbar($page);
        $this->buildContent($page);
        $this->sessionControl($page);

        $this->buildFooter($page);

        ob_end_flush();
        if (isset($_SESSION['Error'])) {
            unset($_SESSION['Error']);
        }
    }
}
