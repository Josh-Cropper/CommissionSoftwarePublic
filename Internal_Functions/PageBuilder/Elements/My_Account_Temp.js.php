<?php include_once __DIR__ . "/../../Includes.php"; ?>

function checkPassValLength() {
    var self = this;
    setTimeout(function () {
        if ($(self).val().length == 6) {
            $('#pswd-send').attr({
                'onclick': 'checkPassVerification();'
            });
            $('#pswd-send').text("Check Code");
        } else {
            $('#pswd-send').attr({
                'onclick': 'getVerification(\'pass\');'
            });
            $('#pswd-send').text("Request Code");
        }
    }, 0);
}

function checkEmailValLength() {
    var self = this;
    setTimeout(function () {
        if ($(self).val().length == 6) {
            $('#email-send').attr({
                'onclick': 'checkEmailVerification();'
            });
            $('#email-send-text').text("Check Code");
        } else {
            $('#email-send').attr({
                'onclick': 'getVerification(\'email\');'
            });
            $('#email-send-text').text("Request Code");
        }
    }, 0);
}

<?php if(isset($_SESSION['Success'])):
    if($_SESSION['Success']['Type'] == "passwordSet"): ?>
        $(window).load(function () {
            $('#passWarn').attr({
                'class': "alert alert-success",
                'hidden': false
            });
            $('#passWarn').html("<?php print $_SESSION['Success']['Message'];?>");
        });
    <?php elseif($_SESSION['Success']['Type'] == "emailSet"): ?>
        $(window).load(function () {
            $('#emailWarn').attr({
                'class': "alert alert-success",
                'hidden': false
            });
            $('#emailWarn').html("<?php print $_SESSION['Success']['Message'];?>");
        });
    <?php elseif($_SESSION['Success']['Type'] == "usernameSet"): ?>
        $(window).load(function () {
            $('#usernameWarn').attr({
                'class': "alert alert-success",
                'hidden': false
            });
            $('#usernameWarn').html("<?php print $_SESSION['Success']['Message'];?>");
        });
    <?php endif;
endif; ?>

$(document).ready(function () {

    $("#passToken").keyup(checkPassValLength);
    $("#passToken").on('paste', checkPassValLength);

    $("#emailToken").keyup(checkEmailValLength);
    $("#emailToken").on('paste', checkEmailValLength);

    $(document).on('change', ':file', function () {
        var input = $(this);
        numFiles = input.get(0).files ? input.get(0).files.length : 1;
        size = input.get(0).files[0].size;
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label, size]);
    });

    $(':file').on('fileselect', function (event, numFiles, label, size) {

        var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (numFiles > 0 && size < 65535) {
            $("[name='avatarSubmit']").attr({
                'disabled' : false
            });
            $('#sizeWarn').hide()
        } else if (size >= 65535) {
            $("[name='avatarSubmit']").attr({
                'disabled' : true
            });
            $('#sizeWarn').show();
        } else {
            $("[name='avatarSubmit']").attr({
                'disabled' : true
            });
            $('#sizeWarn').hide();
        }

        if (input.length) {
            input.val(log);
        } else {
            if (log)
                alert(log);
        }
    });
});

function getVerification(type) {
    if (type == "pass") {
        $.ajax({
            url: "<?=\Settings::_i()->getRoot();?>/Internal_Functions/Request.php",
            method: "POST",
            data: {requestType: 'Sendmail_Account_Alter_Password', passwordChangeEmail: true},
            success: function (data) {
                if (data == "true") {
                    $('#passWarn').attr({
                        'hidden': false
                    });
                    $('#passWarn').html("Email sent");
                } else {
                    $('#passWarn').attr({
                        'hidden': false
                    });
                    $('#passWarn').html("Email failed to send, try checking your spam folder");
                }
            }
        });
    } else if (type == "email") {
        // $.ajax({
        //     url: "",
        //
        // });
        $.get("<?=\Settings::_i()->getRoot();?>/Internal_Functions/Request.php", {requestType: 'Account_Alter_Verification_Email'}, function (data) {
            if (data == "true") {
                $('#emailWarn').attr({
                    'class': "alert alert-success",
                    'hidden': false
                });
                $('#emailWarn').html("Email sent");
            } else {
                $('#emailWarn').attr({
                    'class': "alert alert-danger",
                    'hidden': false
                });
                $('#emailWarn').html("Email failed to send, try checking your spam folder");
            }
        });
    } else if (type == "username") {

    }
    return false;
}

function checkPassVerification() {
    var token = $('#passToken').val();
    $.get("<?=\Settings::_i()->getRoot();?>/Internal_Functions/Request.php",
        {requestType: 'Account_Alter_Verification_Password', passwordChangeVerification: token}, function (data) {
        if (data == "true") {
            $("#passToken").unbind('paste', checkPassValLength);
            $("#passToken").unbind('keyup', checkPassValLength);
            $('#passToken').parent().find("[name='vCode']").val(token);
            $('#passToken').attr({
                'placeholder': "New Password",
                'name': "newPassword",
                'minlength' : "8"
            });
            $('#passToken').val("");
            $('#pswd-send').attr({
                'onclick': null
            });
            $('#passWarn').attr({
                'class': "alert alert-success",
                'hidden': false
            });
            $('#passWarn').html("Verification Successful");

            $("#passToken").keyup(function() {
                if($(this).val().length >= 8) {
                    $('#passwordChange').attr({
                        'disabled' : false,
                        'class' : "btn btn-primary btn-lg"
                    });
                } else {
                    $('#passwordChange').attr({
                        'disabled' : true,
                        'class' : "btn btn-primary btn-lg disabled"
                    });
                }
            });

        } else {
            $('#passWarn').attr({
                'class': "alert alert-danger",
                'hidden': false
            });
            $('#passWarn').html("Verification code does not exist");
        }
    });
    return false;
}

function checkVerification(type) {
    var token = $('#'+type+'Token').val();
    ucType = type.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });

    $.ajax({
        url: "",
        method: "POST",
        data: {requestType: 'Account_Alter_Verification_'+ucType, token: token},
        dataType: "JSON",
        success: function (data) {
            if (data['success']) {
                $("#"+type+"Token").unbind('paste', checkPassValLength);
                $("#"+type+"Token").unbind('keyup', checkPassValLength);
                $("#"+type+"Token").parent().find("[name='vCode']").val(token);
                $("#"+type+"Token").attr({
                    'placeholder': "New Password",
                    'name': "newPassword",
                    'minlength' : "8"
                });
                $('#passToken').val("");
                $('#pswd-send').attr({
                    'onclick': null
                });
                $('#passWarn').attr({
                    'class': "alert alert-success",
                    'hidden': false
                });
                $('#passWarn').html("Verification Successful");

                $("#passToken").keyup(function() {
                    if($(this).val().length >= 8) {
                        $('#passwordChange').attr({
                            'disabled' : false,
                            'class' : "btn btn-primary btn-lg"
                        });
                    } else {
                        $('#passwordChange').attr({
                            'disabled' : true,
                            'class' : "btn btn-primary btn-lg disabled"
                        });
                    }
                });

            } else {
                $('#passWarn').attr({
                    'class': "alert alert-danger",
                    'hidden': false
                });
                $('#passWarn').html("Verification code does not exist");
            }
        }
    });
    $.get("<?=\Settings::_i()->getRoot();?>/Internal_Functions/Request.php",
        {requestType: 'Account_Alter_Verification_Password', passwordChangeVerification: token}, function (data) {

        });
    return false;
}

function checkEmailVerification() {
    var token = $('#emailToken').val();
    $.get("<?=\Settings::_i()->getRoot();?>/Internal_Functions/Request.php", {emailChangeVerification: token}, function (data) {
        if (data == "true") {
            $("#emailToken").unbind('paste', checkEmailValLength);
            $("#emailToken").unbind('keyup', checkEmailValLength);
            $('#vCode').val(token);
            $('#emailToken').attr({
                'placeholder': "New Email",
                'name': "newEmail",
                'type' : "email"
            });
            $('#emailToken').val("");
            $('#email-send').attr({
                'onclick': null
            });
            $('#emailWarn').attr({
                'class': "alert alert-success",
                'hidden': false
            });
            $('#emailWarn').html("Verification Successful");

            $("#emailToken").keyup(function() {
                if(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(this).val())) {
                    $('#emailChange').attr({
                        'disabled' : false,
                        'class' : "btn btn-primary btn-lg"
                    });
                } else {
                    $('#emailChange').attr({
                        'disabled' : true,
                        'class' : "btn btn-primary btn-lg disabled"
                    });
                }
            });

        } else {
            $('#emailWarn').attr({
                'class': "alert alert-danger",
                'hidden': false
            });
            $('#emailWarn').html("Verification code does not exist");
        }
    });
    return false;
}