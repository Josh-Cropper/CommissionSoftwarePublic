<?php
$sql = \Database\getPDOConnection()->prepare("SELECT page FROM Chat_Sessions WHERE user_ID = ? LIMIT 1");
$sql->execute([$_SESSION['User']['ID']]);
if ($sql->rowCount()) {
    if ($sql->fetchColumn() != $_SERVER['PATH_INFO']) {
        $sql = \Database\getPDOConnection()->prepare("UPDATE Chat_Sessions SET page = ? WHERE user_ID = ?");
        $sql->execute([$_SERVER['PATH_INFO'], $_SESSION['User']['ID']]);
    }
} else {
    $sql = \Database\getPDOConnection()->prepare("INSERT INTO Chat_Sessions (page, user_ID) VALUES (?,?)");
    $sql->execute([$_SERVER['PATH_INFO'], $_SESSION['User']['ID']]);
}
?>
    <div class="container centered" style="border: 1px solid red;">
        <div class="card" style="display: inline-block">
            <div class="content" style="font-size: 0px;">
                <i id="connected" class="font-14 fa fa-circle"
                   style="color: darkred; position: absolute; top: 20px; right: 20px;"></i>
                <ul style="display: block" id="log">
                    <?php
                    $chat = \Database\getPDOConnection()->prepare("SELECT username, message FROM Chat_Messages
                                                                         LEFT JOIN Users ON user_ID=Users.ID
                                                                         WHERE page = ? ORDER BY timestamp ASC LIMIT 50");
                    $chat->execute([
                        \Settings::_i()->getRoot() . $_SERVER['PATH_INFO']
                    ]);
                    while ($row = $chat->fetch(PDO::FETCH_ASSOC)): ?>
                        <li><span><?= $row['username'] ?></span>: <?= $row['message'] ?></li>
                    <?php endwhile; ?>
                </ul>
                <div class="attached-inputs">
                    <input type="text" id="msg" onkeydown="onkey(event)" class="attached-left">
                    <button type='button' class='attached-right' onclick="send()">Send</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var socket;
        document.cookie = "PAGE=" + window.location.pathname + "; path=/;";

        messages = [];

        function init() {
            var host = "wss://roll6.com:8999"; // SET THIS TO YOUR SERVER
            try {
                socket = new WebSocket(host);
                // log('WebSocket - status ' + socket.readyState);
                socket.onopen = function (msg) {
                    $("#connected").css({'color': 'green'});
                };
                socket.onmessage = function (msg) {
                    log(msg.data);
                };
                socket.onclose = function (msg) {
                    $("#connected").css({'color': 'darkred'});
                };
            }
            catch (ex) {
                log(ex);
            }
            //$("#msg").focus();
        }

        function send() {
            var txt, msg;
            txt = $("#msg");
            msg = txt.val();
            if (!msg) {
                alert("Message can not be empty");
                return;
            }
            txt.val("");
            txt.focus();
            try {
                socket.send(msg);
                //messages[] = msg;
            } catch (ex) {
                log(ex);
            }
        }

        function quit() {
            if (socket != null) {
                log("Goodbye!");
                socket.close();
                socket = null;
            }
        }

        function reconnect() {
            quit();
            init();
        }

        // Utilities
        // function $(id) {
        //     return document.getElementById(id);
        // }

        function log(msg) {
            var array = JSON.parse(msg);
            if (array['success']) {
                $("#log").append("<li><span>" + array['username'] + "</span>: " + array['message'] + "</li>");
            } else {
                $("#log").append("<li style='color: red;'><span>" + array['username'] + "</span>: " + array['message'] + "</li>");
            }
        }

        function onkey(event) {
            if (event.keyCode == 13) {
                send();
            }
        }

        $(document).ready(function () {
            init();

            //document.domain = "services.runescape.com";

            $.ajax({
                url: '/OvOcommissions/test.php',
                data: {item: 1745},
                type: 'POST',
                dataType: 'json',
                success: function (json) {
                    console.log("TEST");
                }
            });

        });
    </script>

<?php
//PageBuilder::_i()->buildElement("Modal", "", "myModal", "", <<<HTML
//                <p>Wait... This isn't a submit button...</p>
//HTML
//    , "");
