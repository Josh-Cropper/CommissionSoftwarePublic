<?php

$sql = \Database\getPDOConnection()->prepare("SELECT page FROM Chat_Sessions WHERE user_ID = ? LIMIT 1");
$sql->execute([$_SESSION['User']['ID']]);
if ($sql->rowCount()) {
    if ($sql->fetchColumn() != $_SERVER['PATH_INFO']) {
        $sql = \Database\getPDOConnection()->prepare("UPDATE Chat_Sessions SET page = ? WHERE user_ID = ?");
        $sql->execute([$_SERVER['PATH_INFO'], $_SESSION['User']['ID']]);
    }
} else {
    $sql = \Database\getPDOConnection()->prepare("INSERT INTO Chat_Sessions (page, user_ID) VALUES (?,?)");
    $sql->execute([$_SERVER['PATH_INFO'], $_SESSION['User']['ID']]);
}

$ID = (int)explode("/", INDEX::getPage())[1];
$commission = \Database\Commissions\getCommission($ID);

if (!\Database\Commissions\commissionExists($ID) || ($_SESSION['User']['ID'] != $commission['user_ID'] && $_SESSION['User']['Rank'] != 999)) {
    header("Location: " . \Settings::_i()->getRoot() . "/My_Commissions");
}

$watermark = boolval($commission['status'] != "Completed");
$showPayButton = boolval(!in_array($commission['status'], ['Completed', 'Pending']));

//var_dump(\Database\Images\fetch_commission_images($ID, true));
?>

<style>
    #tab1:checked ~ .tab-holder > #initReq,
    #tab2:checked ~ .tab-holder > #chat {
        display: inline-block;
    }
</style>

<div class='commission-container'>
    <div class="commission-sidebar">
        <?php
        $counter = 1;
        foreach (Database\Images\fetch_commission_images($ID, true) as $image): ?>
            <img class="thumbnail hoverable" data-id="<?= $image['ID'] ?>"
                 src="data:image/png;base64,<?= Image_Handler::_i()->showImage($image['data'], 40, 40, false) ?>"
                 title="<?= $image['name'] ?>"/>
            <?php
            $counter++;
        endforeach;

        while ($counter <= $commission['amount']):?>
            <span class="thumbnail hoverable"><?= $counter ?></span>
            <?php
            $counter++;
        endwhile; ?>
    </div>
<!--    <div class="card" style="width: 730px;">-->
<!--        <div class="content" style="min-height: 110px;">-->
<!--            <div class="font-14">Commission Information</div>-->
<!--            <div class="status">-->
<!--                <div class="decision --><?//=$commission['decision']?><!--">--><?//=$commission['decision']?:"Pending"?><!--</div>-->
<!--                <div class="progress"></div>-->
<!--                <div class="payment">--><?//=ucwords($commission['status'])?><!--</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="card" style="width: 730px; min-height: 150px; display: inline-block;">
        <div class="content" style="min-height: 150px;">
            <div style="text-align: right; font-weight: bold;"><input class="responsive clean" type="text" data-imageID="0" id="imageTitle" value="All Images" readonly></div>
            <div class="font-14">Image History</div>
            <div id="imageHistory" style="font-size: 0px;">
                <?php foreach (Database\Images\fetch_commission_images($ID, false) as $image): ?>
                    <img data-name="<?= $image['name'] ?>"
                         src="data:image/png;base64,<?= Image_Handler::_i()->showImage($image['data'], 90, 0, $watermark) ?>"
                         title="<?= $image['name'], " Version: ", $image['version'] ?>"/>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="card" style="width: 730px; display: inline-block;">
        <input class="tab" id="tab1" type="radio" name="tabs" checked>
        <label class="font-14 tab" for="tab1">Initial Requirements</label>
        <input class="tab" id="tab2" type="radio" name="tabs">
        <label class="font-14 tab" for="tab2">Chat</label>
        <div class="tab-holder">
            <div id="initReq" class="tab content" style="min-height: 380px;">
                <textarea
                        style="display: inline-block; width: 100%; max-width: 100%; resize: vertical;"
                        readonly><?= $commission['specification_text'] ?></textarea>
            </div>
            <div id="chat" class="tab content" style="font-size: 0px; min-height: 380px;">
                <i id="connected" class="font-14 fa fa-circle"
                   style="color: darkred; position: absolute; top: 10px; right: 10px;"></i>
                <ul style="display: block" id="log">
                    <?php
                    $chat = \Database\getPDOConnection()->prepare("SELECT username, message FROM Chat_Messages
                                                                         LEFT JOIN Users ON user_ID=Users.ID
                                                                         WHERE page = ? ORDER BY timestamp ASC LIMIT 50");
                    $chat->execute([
                        \Settings::_i()->getRoot() . $_SERVER['PATH_INFO']
                    ]);
                    while ($row = $chat->fetch(PDO::FETCH_ASSOC)): ?>
                        <li><span><?= $row['username'] ?></span>: <?= $row['message'] ?></li>
                    <?php endwhile; ?>
                </ul>
                <div class="attached-inputs" style="width: 100%">
                    <input type="text" id="msg" onkeydown="onkey(event)" class="attached-left">
                    <button type='button' class='attached-right' onclick="send()">Send</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        <?php if($showPayButton && ($_SESSION['User']['ID'] == $commission['user_ID'])): ?>
        $.ajax({
            url: '<?= \Settings::_i()->getRoot() ?>/Internal_Functions/Requests/Content_Requests.php',
            data: {build_element: "Paypal", build_element_args: [<?= $ID; ?>]},
            method: 'POST',
            success: function (data) {
                $('ul.navbar').append("<li class='navbar-list-right' id='payNow'></li>");
                $('#payNow').append(data);
            }
        });
        <?php endif; ?>

        var images = [];
        $("img.thumbnail").on('click', function (event) {
            var id = $(this).attr("data-id");
            if (id in images) {
                console.log(images);
                $("#imageTitle").val(images[id][1]['name']);
                $("#imageTitle").prop("readonly", false);
                $("#imageTitle").data("imageID", false);
                $("#imageTitle").attr({"size": $("#imageTitle").val().length + 1});
                $("#imageHistory").empty();
                $(images).each(function (i) {
                    $(images[i]).each(function (v) {
                        $("#imageHistory").append("<img src=\"data:image/png;base64," + images[i][v]['data'] + "\"/>");
                    });
                });
            } else {
                $.ajax({
                    method: "POST",
                    url: "<?=\Settings::_i()->getRoot()?>/Internal_Functions/Requests/Content_Requests.php",
                    data: {id: id, commission_id: <?=$ID?>},
                    dataType: 'json',
                    success: function (data) {
                        images[id] = data;
                        $("#imageTitle").val(data[id][1]['name']);
                        $("#imageTitle").attr({"size": $("#imageTitle").val().length + 1});
                        $("#imageHistory").empty();
                        $(data).each(function (i) {
                            $(data[i]).each(function (v) {
                                $("#imageHistory").append("<img src=\"data:image/png;base64," + data[i][v]['data'] + "\" title=\"" + data[i][v]['name'] + " Version: " + data[i]['version'] + "\"/>");
                            });
                        });
                    }
                });
            }
        });
    });
    $("label[for='tab2']").one('click', function (event) {
        setTimeout(function () {
            $('#log').scrollTop($('#log')[0].scrollHeight);
        },1);
    });
</script>
<script type="text/javascript">
    var socket;
    document.cookie = "PAGE=" + window.location.pathname + "; path=/;";

    messages = [];

    function init() {
        var host = "wss://roll6.com:8999"; // SET THIS TO YOUR SERVER
        try {
            socket = new WebSocket(host);
            // log('WebSocket - status ' + socket.readyState);
            socket.onopen = function () {
                $("#connected").css({'color': 'green'});
            };
            socket.onmessage = function (msg) {
                log(msg.data);
            };
            socket.onclose = function () {
                $("#connected").css({'color': 'darkred'});
            };
        }
        catch (ex) {
            log(ex);
        }
        //$("#msg").focus();
    }

    function send() {
        var txt, msg;
        txt = $("#msg");
        msg = txt.val();
        if (!msg) {
            return;
        } else {
            txt.val("");
        }
        txt.focus();
        try {
            socket.send(msg);
        } catch (ex) {
            log(ex);
        }
    }

    function quit() {
        if (socket != null) {
            log("Goodbye!");
            socket.close();
            socket = null;
        }
    }

    function reconnect() {
        quit();
        init();
    }

    // Utilities
    // function $(id) {
    //     return document.getElementById(id);
    // }

    function log(msg) {
        var array = JSON.parse(msg);
        if (array['success']) {
            $("#log").append("<li><span>" + array['username'] + "</span>: " + array['message'] + "</li>");
        } else {
            $("#log").append("<li style='color: red;'><span>" + array['username'] + "</span>: " + array['message'] + "</li>");
        }
        $("#log").animate({scrollTop: $("#log").prop("scrollHeight")}, 1000);
    }

    function onkey(event) {
        if (event.keyCode == 13) {
            send();
        }
    }

    $(document).ready(function () {
        init();
    });
</script>