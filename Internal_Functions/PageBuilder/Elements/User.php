<?php
echo $ID = intval(explode("/", INDEX::getPage())[1]);
?>

<div class='container container-1000 set-1000'>
    <table class="table-style" style="width: 100%;">
        <thead>
        <tr>
            <th id="table-title" colspan="6">Temp. Single User Page</th>
        </tr>
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>Admin</th>
            <th>Avatar</th>
            <th>Commissions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach (Database\Account\getUsers($ID, 50, 0, true) as $user): ?>
            <tr class="clickable" link="User/<?= $user['ID'] ?>">
                <td><?= $user['ID'] ?></td>
                <td><?= $user['username'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['rank'] ?></td>
                <td><?= $user['avatar'] ?></td>
                <td>Not a button.</td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>