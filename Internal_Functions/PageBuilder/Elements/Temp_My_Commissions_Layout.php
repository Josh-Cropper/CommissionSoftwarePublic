<?php
$types = Additional_Functions\getCommissionTypes();
if (!$types['success']) {
    echo $types['result'];
    unset($types['result']);
}

?>
<script>
    prependNewCommission();
    setTypes(<?= json_encode($types['result']); ?>);
</script>

<div class='container container-1000 set-1000'>
        <table class="table-style" style="width: 100%;">
            <thead>
            <tr>
                <th class="table-title" colspan="6">Commissions</th>
            </tr>
            <tr>
                <th>ID</th>
                <th>State</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <?php
            try {
                foreach (Database\Commissions\getCommissions($_SESSION['User']['ID'])['result'] as $commission):
                    $statusString = ($commission['accept'] == null ? "Pending For: " :
                        ($commission['final'] == null ? "Accepted" : "Completed"));
                    $buttonStatus = $commission['accept'] == null;
                    if ($statusString == "Pending For: ") {
                        $dateDiff = round(abs(strtotime($commission['time']) - time()) / 3600);
                        $statusString .= $dateDiff . " hours";
                    }
                    ?>
                    <tr class="clickable" link="Commission/<?= $commission['ID'] ?>">
                        <td><?= $commission['ID'] ?></td>
                        <td><?= $statusString ?></td>
                        <td><?= $commission['type'], ($commission['numOf'] < 2 ? "" : " x" . $commission['numOf']) ?></td>
                        <td><?= substr($commission['descr'], 0, 49), strlen($commission['descr']) < 50 ? "" : "..." ?></td>
                    </tr>
                <?php endforeach;
            } catch (Exception $e) {
                echo Database\Commissions\getCommissions($_SESSION['User']['ID'])['result'];
            } ?>
            </tbody>
        </table>
    </div>

    <?php

    $modalContent = <<<HTML
<form class="form-horizontal" style="margin: 0;" requestType="Commission_Create" method="post">
                        <h4>Commission Type:</h4>
                        <select id="commissionType" onchange="commissionTypeChange(this)" style="margin-bottom: 10px;" class="form-control" name="type" required>
                            <option disabled selected value>Choose a Commission Type</option>
HTML;

    foreach ($types['result'] as $type) {
        $modalContent .= <<<HTML
                        <option value="{$type['ID']}">{$type['Name']}</option>
HTML;
    }

    $modalContent .= <<<HTML
    </select>
    <h4>Number:</h4>
    <input id="numItems" onchange="priceUpdate();" name="numOf" class="form-control" type="number" value="1" min="1" required/>
    <h4>Description: <span style='font-size: medium; opacity: 0.5;'>Please be as detailed as possible, this is your initial requirements. An undetailed description will result in your commission not being accepted.</span></h4>
    <textarea style="resize: vertical;" class="form-control" type="text" rows="10" placeholder="Description" name="descr" required></textarea>
    <button style="margin-top: 10px;" class="btn btn-primary btn-lg" type="submit" name="addCommission">Submit</button>
    </form>
HTML;

    PageBuilder::_i()->buildElement("Modal", "newCommission", "New Commission", $modalContent, "<h3 id=\"price\">Price: £0.00</h3>");
    ?>
