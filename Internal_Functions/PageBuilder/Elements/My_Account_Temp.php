<div class="container-600 max-1200">
    <div class="card set-1200">
        <div class="content">
            <table class="font-14">
                <tr>
                    <td rowspan="3">
                        <?php if (isset($_SESSION['User']['Avatar'])): ?>
                            <img src="data:image/png;base64,
                                 <?= Image_Handler::_i()->showImage($_SESSION['User']['Avatar'], 150, 150, false) ?>"/>
                        <?php else: ?>
                            <span class="fa fa-user" style="font-size: 150px; width: 150px;"></span>
                        <?php endif; ?>
                    </td>
                    <td style="padding-left: 15px; text-align: left;">
                        <strong>Username:</strong> <?= $_SESSION['User']['Username']; ?>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 15px; text-align: left;">
                        <strong>User ID:</strong> <?= $_SESSION['User']['ID']; ?>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 15px; text-align: left;">
                        <strong>Email:</strong> <?= $_SESSION['User']['Email']; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="card set-600">
        <div class="content">
            <h4 class="font-14">Change Avatar</h4>
            <form class="form-horizontal" style="margin-bottom: 0;" enctype="multipart/form-data"
                  requestType="Account_Alter_Avatar" method="post" name="changer">
                <div>
                    <input name="userID" type="number" value="<?= $_SESSION['User']['ID']; ?>" hidden readonly>
                    <div class="attached-inputs">
                        <input class="attached-left" type="text" readonly>
                        <label class="file-button attached-right button">
                            Browse
                            <input name="image" accept="image/png" type="file" required>
                        </label>
                    </div>
                </div>
                <div id="sizeWarn" class="font-14"
                     style="margin: 15px 0px 0px 0px; padding: 8px; position: absolute; width: calc(100% - 160px);"
                     hidden>
                    Maximum avatar size is 65KB
                </div>
                <button disabled style="margin-top: 15px; float: right;" type="submit"
                        name="avatarSubmit">Submit
                </button>
            </form>
        </div>
    </div>
    <div class="card set-600">
        <div class="content">
            <h4 class="font-14">Change Username</h4>
            <form class="form-horizontal" style="margin: 0; font-size: 0px;" requestType="Account_Alter_Username"
                  method="post">
                    <div class="verify username">
                    </div>
            </form>
        </div>
    </div>
    <div class="card set-600">
        <div class="content">
            <h4 class="font-14">Change Email Address</h4>
            <form class="form-horizontal" style="margin: 0;" requestType="Account_Alter_Email" method="post">
                <div class="verify email">
                </div>
            </form>
        </div>
    </div>
    <div class="card set-600">
        <div class="content">
            <h4 class="font-14">Change Password</h4>
            <form class="form-horizontal" style="margin: 0;" requestType="Account_Alter_Password" method="post">
                <div class="verify password">
                </div>
            </form>
        </div>
    </div>
    <div class="card set-1200" style="opacity: 0.6;">
        <div class="content">
            <h4 class="font-14">Edit Profile</h4>
        </div>
    </div>
</div>

<?php
if (isset($_SESSION['Success'])) {
    echo $_SESSION['Success']['Message'];
}