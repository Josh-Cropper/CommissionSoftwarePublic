<!DOCTYPE html>
<html>
<head>
    <meta charset='UTF-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= \Settings::_i()->getRoot() ?>/Design/font-awesome/css/font-awesome.css">
    <?php
    $styles = $page->style;
    foreach ($styles as $style) {
        $includes = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . \Settings::_i()->getRoot() . "/Design/Themes/" . $style . "/includes.xml");
        foreach ($includes->css as $css): ?>
            <link rel='stylesheet' type='text/css'
                  href='<?= \Settings::_i()->getRoot(),'/Design/Themes/',$style,'/',$css?>.css'>
        <?php endforeach;
    }
    ?>
    <script src='<?= \Settings::_i()->getRoot() ?>/Internal_Functions/PageBuilder/Elements/_Core/jquery-3.0.0.min.js'></script>
    <script src='<?= \Settings::_i()->getRoot() ?>/Internal_Functions/PageBuilder/Elements/_Core/main.js.php'></script>

    <title><?= \Settings::_i()->getName() . " " . str_replace('_', ' ', INDEX::getPage()); ?></title>
</head>
<body>