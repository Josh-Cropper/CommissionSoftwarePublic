<?php
if(\Functions\sessionCheck()) {
    PageBuilder::_i()->buildElement("Modal", "logoutModal", "Logout", <<<HTML
                        <form requestType="Account_Logout">
                            <button type="submit" class="logout">Logout</button>
                        </form>
HTML
        , "");

} else {
    PageBuilder::_i()->buildElement("Modal", "loginModal", "Login", <<<HTML
                        <form requestType="Account_Login" class="form-horizontal" style="width: 100%;">
                            <h4 class="font-14">Username:</h4>
                            <input style="display: block;" type="text" placeholder="Username" name="username" required />
                            <h4 class="font-14">Password:</h4>
                            <input style="display: block; margin-bottom: 10px;" type="password" placeholder="Password" name="password" required />
                            <button style="display: block;" class="btn btn-primary btn-lg col-xs-4" type="submit">Enter</button>
                            <a class="font-14" href="#" id="forgotPassword" style="margin: 12px; display: inline-block;">Forgot Password</a>
                        </form>
HTML
        , "");

    $error = "";
    if (isset($_SESSION['Error']['Type']) && $_SESSION['Error']['Type'] == "create_account") {
        $error = "<span class='fa fa-exclamation-triangle' style='color: red'></span>";
    }

    PageBuilder::_i()->buildElement("Modal", "createAccountModal", "Create Account", <<<HTML
                        <form requestType="Account_Create" class="form-horizontal" style="width: 100%;">
                            {$error}
                            <h4 class="font-14">Username:</h4>
                            <input class="form-control input-lg" style="margin: 0 0 10px;" type="text" placeholder="Username" name="username" required />
                            <h4 class="font-14">Email:</h4>
                            <input class="form-control input-lg" style="margin: 0 0 10px;" type="email" placeholder="Email" name="email" required />
                            <!--<strong>ALERT: </strong>Please be sure to check your spam box!-->
                            <button type="submit">Enter</button>
                        </form>
HTML
        , "");
}
?>