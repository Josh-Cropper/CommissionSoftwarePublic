    </body>
    <footer>
<?php
$styles = $page->style;
foreach ($styles as $style) {
    $includes = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . \Settings::_i()->getRoot() . "/Design/Themes/" . $style . "/includes.xml");
    foreach ($includes->js as $js) {
        echo <<<HTML
        <script src='{\Settings::_i()->getRoot()}/Design/Themes/{$style}/{$js}.js'></script>\n
HTML;
    }
}
?>
    </footer>
</html>