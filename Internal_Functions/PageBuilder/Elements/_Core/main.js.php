<?php include_once(__DIR__ . "../../../../Includes.php"); ?>

$.fn.switchClass = function (addClasses, removeClasses) {
    if (removeClasses instanceof Array) {
        $.each(removeClasses, function (i) {
            $(this).removeClass(removeClasses[i]);
        });
    } else {
        $(this).removeClass(removeClasses);
    }
    if (addClasses instanceof Array) {
        $.each(addClasses, function (i) {
            $(this).addClass(addClasses[i]);
        });
    } else {
        $(this).addClass(addClasses);
    }
};

getCookie = function (name) {
    match = document.cookie.match(new RegExp(name + '=([^;]+)'));
    if (match) return match[1];
}

function isset(vari) {
    if ($.inArray(vari, [undefined, null, ''])) {
        return false;
    } else if (vari instanceof Array && !vari.length) {
        return false;
    }
    return true;
}

var codeRequested = [];

$(document).on('submit', 'form[requestType]', function (event) {
    event.preventDefault();
    var self = $(event.target);
    var form = new FormData(this);
    form.append('requestType', self.attr('requestType'));
    $.ajax({
        url: "<?=\Settings::_i()->getRoot()?>/Internal_Functions/Request.php",
        data: form,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'JSON',
        success: function (data) {
            eval(data.response);
        }
    });
});

$(document).on('keyup', "div.verify input[name$='Token'], input[type='text'].responsive", function (event) {
    var self = $(event.target);
    if (self.is("div.verify input[name$='Token']")) {
        if (!$(event.target).siblings("button[name$='TokenSend']").is(":disabled")) {
            setTimeout(function () {
                if ($(event.target).val().length == 6) {
                    $(event.target).siblings("button[name$='TokenSend']").text("Check Code");
                } else {
                    $(event.target).siblings("button[name$='TokenSend']").text("Request Code");
                }
            }, 0);
        }
    } else if (self.is("input[type='text'].responsive")) {
        $("input[type='text'].responsive").attr({"size": $("input[type='text'].responsive").val().length + 1});
    }
});

$(document).on('click', "#forgotPassword, div.verify button[name$='TokenSend'], div.submitGroup > [type='submit'], table.table-style > tbody > tr.clickable > td", function (event) {
    var self = $(event.target);
    if (self.is('#forgotPassword')) {

        if ($("#loginModal [name='username']")[0].checkValidity()) {
            $.ajax({
                url: "<?=\Settings::_i()->getRoot()?>/Internal_Functions/Request.php",
                method: "POST",
                data: {
                    requestType: "Sendmail_Account_Reset_Password",
                    passwordResetEmail: true,
                    username: $("#loginModal [name='username']").val()
                },
                dataType: "JSON",
                success: function (data) {
                    console.log(data);
                }
            });
        }
        else {
            $("#loginModal [name='password']").prop("required", false);
            $("#loginModal [name='username']").parent().find(":submit").click();
        }
    } else if (self.is("div.verify button[name$='TokenSend']")) {

        var type = self.attr('name').replace(/TokenSend/, '');
        var Type = type.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        if (self.siblings("input[name$='Token']").val().length == 6) {
            var token = $(event.target).siblings("input[name$='Token']").val();
            $.ajax({
                url: "<?= \Settings::_i()->getRoot() ?>/Internal_Functions/Request.php",
                method: "POST",
                data: {requestType: 'Account_Alter_Verification_' + Type, token: token},
                dataType: "JSON",
                success: function (data) {
                    if (data) {
                        self.parent().siblings("input[name='vCode']").val(token);

                        self.siblings("input[name$='Token']").val('');
                        self.siblings("input[name$='Token']").attr({'placeholder': "New " + Type + (Type == 'Email' ? ' Address' : '')});
                        if (type == 'password') {
                            self.siblings("input[name$='Token']").attr({'type': 'password'});
                        }

                        self.parent().siblings(".submitGroup").find("[type='submit']").prop('disabled', false);
                        self.parent().siblings(".submitGroup").find("#" + type + "Warn").toggleClass('good');
                        self.parent().siblings(".submitGroup").find("#" + type + "Warn").css({'visibility': 'visible'});
                        self.prop('disabled', true);
                    }
                }
            });
        } else if (!isset(codeRequested[type]) || codeRequested[type] != true) {
            $.ajax({
                url: "<?= \Settings::_i()->getRoot() ?>/Internal_Functions/Request.php",
                method: "POST",
                data: {requestType: 'Sendmail_Account_Alter_' + Type},
                dataType: "JSON",
                success: function (data) {
                    if (data['success']) {
                        self.siblings(".submitGroup").find("#" + type + "Warn").switchClass('good', ['bad', 'moderate']);
                        //self.siblings(".submitGroup").find("#" + type + "Warn").removeClass('moderate');
                        //self.siblings(".submitGroup").find("#" + type + "Warn").removeClass('bad');
                        //self.siblings(".submitGroup").find("#" + type + "Warn").addClass('good');
                    } else {
                        self.parent().siblings(".submitGroup").find("#" + type + "Warn").switchClass('bad', ['good', 'moderate']);
                        console.log(data['success']);
                        console.log(type);
                        //self.siblings(".submitGroup").find("#" + type + "Warn").removeClass('good');
                        //self.siblings(".submitGroup").find("#" + type + "Warn").removeClass('moderate');
                        //self.siblings(".submitGroup").find("#" + type + "Warn").addClass('bad');
                    }
                    self.parent().siblings(".submitGroup").find("#" + type + "Warn").css({'visibility': 'visible'});
                    self.parent().siblings(".submitGroup").find("#" + type + "Warn").text(data['message']);

                    codeRequested[type] = true;
                }
            });
        } else {
            self.siblings(".submitGroup").find("#" + type + "Warn").switchClass('moderate', ['bad', 'good']);
            //self.siblings(".submitGroup").find("#" + type + "Warn").removeClass('good');
            //self.siblings(".submitGroup").find("#" + type + "Warn").removeClass('bad');
            //self.siblings(".submitGroup").find("#" + type + "Warn").addClass('moderate');
            self.siblings(".submitGroup").find("#" + type + "Warn").text("Email could not send!");
        }
    } else if (self.is("div.submitGroup > [type='submit']")) {

        var type = self.parent().siblings("button[name$='TokenSend']").attr('name').replace(/TokenSend/, '');
        var token = self.parent().siblings("input[name='vCode']").val();
        var value = self.parent().siblings("input[name$='Token']").val();
        var Type = type.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });

        $.ajax({
            url: "<?= \Settings::_i()->getRoot() ?>/Internal_Functions/Request.php",
            method: "POST",
            data: {requestType: 'Account_Alter_' + Type, token: token, value: value},
            dataType: "JSON",
            success: function (data) {
                if (data['success']) {
                    self.siblings("#" + type + "Warn").toggleClass('good');
                } else {
                    self.siblings("#" + type + "Warn").toggleClass('bad');
                }
                self.siblings("#" + type + "Warn").css({'visibility': 'visible'});
                self.siblings("#" + type + "Warn").text(data['message']);
            }
        });
    } else if (self.is("table.table-style > tbody > tr.clickable > td")) {
        window.document.location = '<?= Settings::_i()->getRoot()?>/' + self.parents(0).attr('link');
        console.log("Test");
    }
});

$(document).ready(function () {
    $(".verify").append("<input type='hidden' name='vCode'>");
    $(".verify").append(
        "<div class='submitGroup'>" +
        "<button type='submit' disabled>Submit</button>" +
        "</div>"
    );

    $(".verify.password").prepend(
        "<div class='attached-inputs'>" +
        "<input type='text' class='attached-left' name='passwordToken' placeholder='Verification Code'>" +
        "<button type='button' class='attached-right' name='passwordTokenSend'>Request Code</button>" +
        "</div>"
    );
    $(".verify.password .submitGroup").prepend("<div id='passwordWarn' style='visibility: hidden'>Verification Accepted</div>");

    $(".verify.username").prepend(
        "<div class='attached-inputs'>" +
        "<input type='text' class='attached-left' name='usernameToken' placeholder='Verification Code'>" +
        "<button type='button' class='attached-right' name='usernameTokenSend'>Request Code</button>" +
        "</div>"
    );
    $(".verify.username .submitGroup").prepend("<div id='usernameWarn' style='visibility: hidden'>Verification Accepted</div>");

    $(".verify.email").prepend(
        "<div class='attached-inputs'>" +
        "<input type='text' class='attached-left' name='emailToken' placeholder='Verification Code'>" +
        "<button type='button' class='attached-right' name='emailTokenSend'>Request Code</button>" +
        "</div>"
    );
    $(".verify.email .submitGroup").prepend("<div id='emailWarn' style='visibility: hidden'>Verification Accepted</div>");

    if (isset($("input[type='text'].responsive"))) {
        $("input[type='text'].responsive").attr({"size": $("input[type='text'].responsive").val().length + 1});
    }
});