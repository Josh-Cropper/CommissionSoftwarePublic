<style>

    .dropdown-content {
        display: none;
        position: fixed;
        background-color: #bbb;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px;
    }

    .dropdown-content a {
        float: none;
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        text-align: left;
    }

    .dropdown-content a:last-child {
        border-bottom: 5px solid #888;
        border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px;
    }

    .dropdown-content a:hover {
        background-color: #d6d6d6;
    }

    body {
        padding-top: 60px;
        height: calc(100% - 60px);
    }

    ul.navbar {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #bbb;
        border-bottom: 5px solid #888 !important;
        min-height: 0 !important;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 2000;
        line-height: 1 !important;
    }

    ul.navbar li {
        float: left;
        color: #333;
    }

    li a {
        display: block;
        color: #333;
        text-align: center;
        padding: 14px 16px !important;
        text-decoration: none !important;
        cursor: pointer;
        border-radius: 5px;
    }

    li a:hover:not(.active) {
        background-color: #d6d6d6;
        color: #333 !important;
    }

    li.navbar-list-right {
        float: right !important;
        cursor: pointer;
    }

    li.navbar-Title {
        position: fixed;
        left: 50%;
        top: 0;
        transform: translateX(-50%);
        cursor: default;
    }

    li.navbar-Title > h2 {
        margin-top: 10px;
        font-weight: normal;
        font-size: 18pt !important;
    }

    .active {
        background-color: #e7e7e7;
        border-radius: 5px;
    }

    ul.navbar > #twitch > a, ul.navbar > #twitch > svg {
        background: #4b367c;
        color: #d7cceb !important;
        fill: #d7cceb;
    }

    .animated {
        animation: colorchange 2s infinite ease-in-out;
        -webkit-animation: colorchange 2s infinite ease-in-out;
    }

    /* Temp Mobile Fix */
    @media screen and (max-width: 700px) {
        body {
            padding-top: 145px;
            height: calc(100% - 145px);
        }

        ul.navbar li {
            float: initial;
            color: #333;
            width: 32%;
            display: inline-block;
        }
        ul.navbar li.navbar-list-right {
            float: initial !important;
            cursor: pointer;
        }
        li a {
            font-size: 10pt;
            padding: 14px 6px !important;
        }
        li.navbar-Title {
            display: none !important;
        }
    }

</style>

<?php
if (Additional_Functions\checkTwitch()) {
    $ch = curl_init();

    curl_setopt_array($ch, array(
        CURLOPT_HTTPHEADER => array(
            'Client-ID: ' . 'nk5tj3jq6zpsslngqzm06m8cowypgl'
        ),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => "https://api.twitch.tv/kraken/streams/ovowls"
    ));

    $data = curl_exec($ch);
    curl_close($ch);

    $json_array = json_decode($data, true);

    Additional_Functions\updateTwitch(isset($json_array["stream"]));
}
$navDrops = array();

?>

<ul class="navbar">
    <li class="navbar-Title"><h2><?= \Settings::_i()->getName() ?></h2></li>
    <?php foreach (\Settings::_i()->navigation->tree->branch as $nav):
        if (Functions\checkAccess($nav->attributes()['ranks'], PageBuilder::_i()->getUserRank())):
            if ($nav->attributes()['type'] == 'button'): ?>
                <li><a <?= $nav->pagelink == INDEX::getPage() ? "class=\"active\"" : "" ?>
                            href="<?= \Settings::_i()->getRoot(), "/", $nav->pagelink ?>"><?= $nav->title ?></a></li>
            <?php elseif ($nav->attributes()['type'] == 'list'):
                $active = in_array(INDEX::getPage(), $nav->xpath('branch/pagelink')) ? " active" : "";
                array_push($navDrops, $nav->title); ?>
                <li href="#" class="dropdown<?= $active ?>">
                    <a id="drpdn-btn-<?= $nav->title ?>"
                       onclick="$('#drpdn-<?= $nav->title ?>').toggle();"><?= $nav->title ?> <span
                                onclick="$('#drpdn-<?= $nav->title ?>').toggle();" id="drpdn-caret-<?= $nav->title ?>"
                                class="fa fa-caret-down"></span></a>
                    <div class="dropdown-content" id="drpdn-<?= $nav->title ?>">
                        <?php foreach ($nav->branch as $branch):
                            if (Functions\checkAccess($branch->attributes()['ranks'], PageBuilder::_i()->getUserRank())): ?>
                                <a href="<?= \Settings::_i()->getRoot(), "/", $branch->pagelink ?>"><?= $branch->title ?></a>
                            <?php endif;
                        endforeach; ?>
                    </div>
                </li>
            <?php
            endif;
        endif;
    endforeach; ?>
    <li id="twitch">
        <a target="_blank" href="https://www.twitch.tv/ovowls">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 134" width="12px">
                <path d="M89,77l-9,23v94h32v17h18l17-17h26l35-35V77H89Zm107,76-20,20H144l-17,17V173H100V89h96v64Zm-20-41v35H164V112h12Zm-32,0v35H132V112h12Z"
                      transform="translate(-80 -77)"/>
            </svg>
            Twitch.tv
        </a>
    </li>

    <?php if (isset($_SESSION['User'])): ?>
        <li class="navbar-list-right">
            <a href="<?= \Settings::_i()->getRoot() ?>/My_Account">
                <?php if ($_SESSION['User']['Avatar'] != null): ?>
                    <img style="margin: -7px 0;"
                         src="data:image/png;base64,<?= Image_Handler::_i()->showImage($_SESSION['User']['Avatar'], 24, 0, false) ?>"/>
                <?php else: ?>
                    <i class="fa fa-user"></i>
                <?php endif; ?>
                My Account
            </a>
        </li>
        <li class="navbar-list-right"><a onclick="$('#logoutModal').css({'display': 'inline-block'})">Logout</a></li>
    <?php else: ?>
        <li class="navbar-list-right"><a onclick="$('#loginModal').css({'display': 'inline-block'})">Login</a></li>
        <li class="navbar-list-right"><a onclick="$('#createAccountModal').css({'display': 'inline-block'})">Create
                Account</a></li>
    <?php endif; ?>
</ul>
<script>
    $('<?="#drpdn-btn-", implode(", #drpdn-btn-", $navDrops),
    ", #drpdn-", implode(", #drpdn-", $navDrops),
    ", #drpdn-caret-", implode(", #drpdn-caret-", $navDrops)?>').on('click', function (e) {
        console.log(e.target.id);
        console.log($("ul.navbar > li:nth-child(2) > a:first-child").attr('id'));
        if (e.target.id == $("ul.navbar > li:nth-child(2) > a:first-child").attr('id') ||
            e.target.id == $("ul.navbar > li:nth-child(2) > a:first-child > .fa").attr('id')) {
            console.log($("ul.navbar").css("border-bottom-left-radius"));
            if ($("ul.navbar").css("border-bottom-left-radius") == "0px") {
                $("ul.navbar").css({"border-bottom-left-radius": ""});
            } else {
                $("ul.navbar").css({"border-bottom-left-radius": "0px"});
            }
        }
        e.stopPropagation();
    });
    $(document).on('click', function (e) {
        $('<?= "#drpdn-", implode(", #drpdn-", $navDrops) ?>').hide();
        $("ul.navbar").css({"border-bottom-left-radius": ""});
    });
</script>
<?php if (Additional_Functions\getTwitch() == 1): ?>
    <script>
        $(document).ready(function () {
            $("#twitch>a").addClass("animated");
            $("#twitch>a").append(" - LIVE")
        });
    </script>
<?php endif; ?>