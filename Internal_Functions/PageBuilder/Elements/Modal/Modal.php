<?php
$head = ($args[1] == "" || $args[1] == null) ? "&nbsp;" : $args[1];
$body = $args[2];
$footer = ($args[3] == "" || $args[3] == null) ? "&nbsp;" : $args[3];
echo <<<HTML
<div id="{$args[0]}" class="modal-new">

    <!-- Modal content -->
    <div class="modal-content-new">
        <div class="modal-header-new">
            <span onclick="$('#{$args[0]}').css('display','none');" class="close-new">&times;</span>
            <h2>{$head}</h2>
        </div>
        <div class="modal-body-new">
            <div class="content">
            {$body}
            </div>
        </div>
        <div class="modal-footer-new">
            <h3>{$footer}</h3>
        </div>
    </div>

</div>
<script>
$(document).ready(function () {
    $("#{$args[0]}").on('click', function (event) {
    if(event.target.id == "{$args[0]}")
        $('#{$args[0]}').css('display','none');
    });
});
</script>

HTML;
