<?php
include_once __DIR__ . '/../../../Includes.php';

$commissionID = $args[0];
$commission = \Database\Commissions\getCommission($commissionID);


?>
<form name="_xclick" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

    <!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="business" value="<?= \Settings::_i()->paypal ?>">
    <!-- Specify a Buy Now button. -->
    <input type="hidden" name="cmd" value="_xclick">

    <!-- Specify details about the item that buyers will purchase. -->
    <input type="hidden" name="item_name" value="<?= $commission['name'] ?>">
    <input type="hidden" name="amount" value="<?= $commission['price'] ?>">
    <input type="hidden" name="quantity" value="<?= $commission['amount'] ?>">
    <input type="hidden" name="currency_code" value="GBP">
    <input name="notify_url" value="https://roll6.com<?= \Settings::_i()->getRoot() ?>/Internal_Functions/Requests/Paypal_Callback.php"
           type="hidden">
    <input type="hidden" name="return" value="https://roll6.com<?= \Settings::_i()->getRoot() ?>/Commission/<?= $commissionID ?>">
    <input type="hidden" name="custom" value="<?=$commissionID?>">

    <!-- Display the payment button. -->
    <button type="submit" name="submit" class="paypal">
        <span class="fa fa-paypal"></span> Pay Now
    </button>
</form>
