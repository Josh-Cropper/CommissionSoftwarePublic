var types;

function setTypes(types) {
    this.types = types;
}

function prependNewCommission() {
    var new_li = $('<li class="navbar-list-right"></li>');
    new_li.append("<a data-toggle='modal' onclick=\"$('#newCommission').css({'display': 'inline-block'})\"><span class='fa fa-plus-circle'></span> New Commission</a>");
    new_li.appendTo("ul.navbar");
}

function commissionTypeChange(sel) {
    var max = types[sel.value]['Limit'];
    $('#numItems').attr({
        "max": max
    });
    if (max == 1) {
        $('#numItems').attr({
            "readonly": true
        });
    } else {
        $('#numItems').attr({
            "readonly": false
        });
    }

    if ($('#numItems').val() > max) {
        $('#numItems').val(max);
    }
    priceUpdate();
}

function priceUpdate() {
    var price = $('#numItems').val() * types[$('#commissionType').val()]['Price'];
    $('#price').html("Price: £" + price.toFixed(2) + "<span style='font-size: medium; opacity: 0.5;'> roughly $" + (price * 1.2873).toFixed(2) + "</span>");
}