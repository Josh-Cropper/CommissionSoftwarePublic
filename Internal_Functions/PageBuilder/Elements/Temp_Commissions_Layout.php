<?php
set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    throw new RuntimeException($errstr . " on line " . $errline . " in file " . $errfile);
});

if(isset($_POST['decision'])) {
    $sql = \Database\getPDOConnection()->prepare("UPDATE Commissions SET decision_time = NOW(), decision = ? WHERE ID = ?");
    $sql->execute([$_POST['decision']?"Accepted":"Rejected", (int)$_POST['id']]);
}

?>
<div class='container container-1000 set-1000'>
    <table class="table-style" style="width: 100%;">
        <thead>
        <tr>
            <th id="table-title" colspan="6">Commissions</th>
        </tr>
        <tr>
            <th>ID</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Username</th>
            <th>Status</th>
            <th>-</th>
        </tr>
        </thead>
        <tbody>
        <?php
        try {
            foreach (Database\Commissions\getCommissions(0, 0, 0, "decision_time, finalised, timestamp")['result'] as $commission):
                $statusString = $commission['decision']
                    ? !$commission['finalised']
                        ? $commission['decision']
                        : "Completed"
                    : "Pending For: ";
                if ($statusString == "Pending For: ") {
                    $dateDiff = round(abs(strtotime($commission['timestamp']) - time()) / 3600);
                    $statusString .= $dateDiff . " hours";
                }

                ?>
                <tr class="clickable" link="Commission/<?= $commission['ID'] ?>">
                    <td><?= $commission['ID'] ?></td>
                    <td><?= $commission['type'] ?></td>
                    <td><?= $commission['amount'] ?></td>
                    <td><?= $commission['username'] ?></td>
                    <td><?= $statusString ?></td>
                    <td style="width: 1%; white-space: nowrap;">
                        <form method="post">
                            <input name="id" type="hidden" value="<?=$commission['ID']?>">
                        <?php if (!$commission['decision']): ?>
                            <button name="decision" value="1" type="submit">Accept</button>
                            <button name="decision" value="0" type="submit" class="red">Reject</button>
                        <?php else: ?>
                            <button disabled>Accept</button>
                            <button disabled>Reject</button>
                        <?php endif; ?>
                        </form>
                    </td>
                </tr>
            <?php
            endforeach;
        } catch (Exception $e) {
            echo Database\Commissions\getCommissions()['result'];
        }

        ?>
        </tbody>
    </table>
</div>