<div style='text-align: center'>
    <div class='card-holder'>
        <span style="color: white; font-size: 100px; left: 50%; top: 20%; transform: translateX(50%) translateY(50%); position: absolute;"
              class="fa fa-spinner fa-pulse"></span>
    </div>
</div>

<script>
    $(document).ready(function () {
        $.ajax({
            url: "<?= \Settings::_i()->getRoot() ?>/Internal_Functions/Requests/Content_Requests.php",
            method: "POST",
            data: {getGalleryImages: true, galleryPage: 1},
            dataType: "JSON",
            success: function (imageData) {
                $(".card-holder:first-of-type").empty();
                $.each(imageData, function (i) {
                    var name = imageData[i]['name'].replace(/_/g, ' ').replace(".png", "");
                    var username = "";
                    if (imageData[i]['username'] !== null) {
                        username = imageData[i]['username'];
                    }
                    var element = "<div class=\"card hoverable\" style=\"width: 230px; height: 230px; display: inline-block;\">\n" +
                        "                <div class=\"content\">\n" +
                        "                    <table  class=\"font-14\" style=\"height: 180px; width: 100%;\">\n" +
                        "                        <tr>\n" +
                        "                            <td colspan=\"2\">\n" +
                        "                                <img src=\"data:image/png;base64," + imageData[i]['data'] + "\" />\n" +
                        "                            </td>\n" +
                        "                        </tr>\n" +
                        "                        <tr>\n" +
                        "                            <td style=\"text-align: left; vertical-align: bottom;\"><strong>" + username + "</strong></td>\n" +
                        "                            <td style=\"text-align: right; vertical-align: bottom;\">" + name + " - " + imageData[i]['version'] + "</td>\n" +
                        "                        </tr>\n" +
                        "                    </table>\n" +
                        "                </div>\n" +
                        "            </div>";
                    $(".card-holder:first-of-type").append(element);
                });
            }
        });
    });
</script>