<?php
if (!isset($_GET['token'])) {
    header("refresh:10; url=" . \Settings::_i()->getRoot() . "/" . PageBuilder::_i()->getMainPage());

    ?>
    <div class='container-fluid' style='padding-top: 70px;'>
        <div class="card-outer col-xs-12 col-sm-10 col-md-8 col-lg-6
             col-sm-offset-1 col-md-offset-2 col-lg-offset-3">
            <div class="card">
                <div class="content" style='text-align: center'>
                    <h1>Error: Why?</h1>
                    <h3>Why must you mess with your validation link? Why? Begone!</h3>
                    <img src="<?=\Settings::_i()->getRoot()?>/Design/Themes/Default/Images/moSeriously.png" />
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    if ((!\Database\Account_Creation\checkValue(stripslashes($_GET['token']))) && (stripslashes($_GET['token']) != "loginFail")) {

        ?>
        <div class='container-fluid'>
            <div class="card-outer col-xs-12 col-sm-10 col-md-8 col-lg-6
                 col-sm-offset-1 col-md-offset-2 col-lg-offset-3">
                <div class="card">
                    <div class="content" style='text-align: center'>
                        <h1>Some Error Number</h1>
                        <h3>[Cue Twilight Zone Music]: We searched everywhere... Your token... It-it-it it doesn't exist!</h3>
                        <img src="<?=\Settings::_i()->getRoot()?>/Design/Themes/Default/Images/ovoScare.png" />
                        <?php
                    } else {
                        if (stripslashes($_GET['token']) != "loginFail") {
                            $token = stripslashes($_GET['token']);
                            $sql = \Database\getConnection()
                                ->prepare("SELECT username, email FROM Account_Create WHERE token = ?");
                            $sql->bind_param('s', $token);
                            $sql->execute();
                            $sql->store_result();
                            $sql->bind_result($username, $email);
                            $sql->fetch();

                            $sql = \Database\getConnection()
                                ->prepare("SELECT ID FROM Users WHERE username = ?");
                            $sql->bind_param('s', $username);
                            $sql->execute();
                            $sql->store_result();
                            $loginCheck = ($sql->num_rows > 0);
                        }
                        if ($loginCheck || stripslashes($_GET['token']) == "loginFail") {

                            ?>
                            <div class="card-outer col-xs-12 col-sm-10 col-md-8 col-lg-6
                                 col-sm-offset-1 col-md-offset-2 col-lg-offset-3">
                                <div class="card">
                                    <div class="content" style='text-align: center'>
                                        <h1>This Is More Of An Achievement Than An Error</h1>
                                        <h3>Someone literally took your username between you receiving your validation email and you clicking the link... That's mildly impressive.</h3>
                                        <img src="<?= \Settings::_i()->getRoot()?>/Design/Themes/Default/Images/fg8JE1iC.png" />
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else {
                            $password = \Functions\randomString(15);
                            $encrypted = Functions\encryptString($password);
                            if (!($sql = \Database\getConnection()->prepare("INSERT INTO Users(username, email, password) VALUES (?,?,?)"))) {

                                ?>
                                <div class="alert alert-danger">
                                      Erm... Wut?
                                </div>
                                <?php
                            }
                            $sql->bind_param('sss', $username, $email, $encrypted);

                            if (!$sql->execute()) {

                                ?>
                                <div class="alert alert-danger">
                                      No sassy remarks... Just genuine confusion...
                                </div>
                                <?php
                            } else {
                                $sql = \Database\getConnection()
                                    ->prepare("DELETE FROM Account_Create WHERE token = ?");
                                $sql->bind_param('s', $value);
                                $sql->execute();

                                $to = '<' . $email . '>';
                                $subject = "Roll6 Account Password";

                                $message = "<h1>Roll6 Account Password</h1>"
                                    . "<p>Your password for account <strong>$username</strong> is:</p><br>"
                                    . "<h2>$password</h2><br>"
                                    . "Login at: <a href='http://roll6.com/OvOcommissions' ><h3>http://roll6.com/OvOcommissions</h3></a>";

                                $headers[] = 'MIME-Version: 1.0';
                                $headers[] = 'Content-type:text/html;charset=UTF-8';
                                $headers[] = 'Return-Path: password@roll6.com \r\n';
                                $headers[] = 'Organization: Roll6 \r\n';
                                $headers[] = 'X-Mailer: PHP/' . phpversion();

                                $headers[] = 'From: Roll6.com <password@roll6.com>';

                                $retval = mail($to, $subject, $message, implode("\r\n", $headers), "-f password@roll6.com");

                                ?>
                                <div class="alert alert-success">
                                      Username: <?php print $username; ?><br>
                                    &nbsp;&nbsp;Password: <?php print $password; ?><br>
                                    &nbsp;&nbsp;We have no means of accessing your password, we will send an email containing your password shortly.
                                    After that, if you lose this password you will have to reset it, it cannot be recovered.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php
                            }
                        }
                    }
                }

                ?>