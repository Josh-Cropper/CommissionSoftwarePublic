<?php

if (isset($_POST['submit'])) {

    $tmpName = $_FILES['image']['tmp_name'];
    $fp = fopen($tmpName, 'r');
    $data = fread($fp, filesize($tmpName));
    fclose($fp);

    Image_Handler::_i()->insertImage($data, $_FILES['image']['name']);
} elseif (isset($_POST['submit2'])) {

    $tmpName = $_FILES['image']['tmp_name'];
    $fp = fopen($tmpName, 'r');
    $data = fread($fp, filesize($tmpName));
    fclose($fp);

    Image_Handler::_i()->insertImageNew($data, 0, $_FILES['image']['name']);
}
?>
<div class='container-fluid'>
    <script type="text/javascript"> ///Credit goes to Cory LaViska for this javascript///
        $(function () {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready(function () {
                $(':file').on('fileselect', function (event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    } else {
                        if (log)
                            alert(log);
                    }

                });
            });

        });
    </script>
    <form enctype="multipart/form-data" style="margin-bottom: 10px;" action="" method="post" name="changer">
            <div class="attached-inputs" style="width: 300px;">
                <input class="attached-left" type="text" readonly>
                <label class="file-button attached-right button">
                    Browse
                    <input name="image" accept="image/png" type="file" required>
                </label>
            </div>
        <input class="button" value="Submit" type="submit" name="submit">
    </form>
    <form enctype="multipart/form-data" action="" method="post" name="changer2">
            <div class="attached-inputs" style="width: 300px;">
                <input class="attached-left" type="text" readonly>
                <label class="file-button attached-right button">
                    Browse
                    <input name="image" accept="image/png" type="file" required>
                </label>
            </div>
        <input class="button" value="Submit (new)" type="submit" name="submit2">
    </form>
</div>