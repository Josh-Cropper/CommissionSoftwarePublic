<?php

include_once("../Includes.php");

if (Functions\checkSet($_REQUEST) && \Functions\checkSet($_REQUEST['custom'])) {
    $price = \Database\getPDOConnection()->prepare("SELECT Commissions.amount*Commission_Types.price FROM Commissions LEFT JOIN Commission_Types ON Commissions.`type` = Commission_Types.ID WHERE Commissions.ID = ?");
    $price->execute([(int)$_REQUEST['custom']]);
    $price = $price->fetchColumn();

    $sql = \Database\getPDOConnection()->prepare("INSERT INTO Paypal_Responses (invoice_ID, response) VALUES (?, '".var_export($_REQUEST, true)."')");
    $sql->execute([(int)$_REQUEST['custom']]);

    $sql = \Database\getPDOConnection()->prepare("UPDATE Commissions
                                                            LEFT JOIN Invoices ON Commissions.invoice=Invoices.ID
                                                            SET Invoices.paid = ? WHERE Commissions.ID = ?");
    $sql->execute([$_REQUEST['payment_status'], (int)$_REQUEST['custom']]);

    if($_REQUEST['payment_status'] == "Completed") {
        if((double)$_REQUEST['mc_gross'] < (double)$price) {
            $sql = \Database\getPDOConnection()->prepare("UPDATE Commissions
                                                            LEFT JOIN Invoices ON Commissions.invoice=Invoices.ID
                                                            SET Invoices.paid = 'partial', Invoices.amount_paid = ? WHERE Commissions.ID = ?");
            $sql->execute([$_REQUEST['mc_gross'], (int)$_REQUEST['custom']]);
        }
    }

}