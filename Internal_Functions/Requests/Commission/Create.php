<?php

if (isset($_POST['addCommission'])) {
    Database\Commissions\add_commission(
        $_SESSION['User']['ID'],
        $_POST['type'],
        $_POST['numOf'],
        htmlentities($_POST['descr'])
    );
    header("Location: ".\Settings::_i()->getRoot()."/My_Commissions");
} else {
    header("Location: " . $_SERVER['HTTP_REFERER']);
}