<?php

if (isset($_POST['emailChange'])) {

    $token = $_POST['vCode'];
    $email = $_POST['newEmail'];
    $user_ID = $_SESSION['User']['ID'];

    if (!$sql = \Database\getConnection()
        ->prepare("SELECT * FROM Account_Edit WHERE token = ? AND user_ID = ? AND type = 'email'")) {

        print "Failed Prepare";

    }

    $sql->bind_param("si", $token, $user_ID);
    $sql->execute();
    $sql->store_result();
    $test = $sql->num_rows;

    if ($test == 1) {

        $sql = \Database\getConnection()
            ->prepare("UPDATE Users SET email = ? WHERE ID = ?");
        $sql->bind_param("si", $email, $user_ID);

        if ($sql->execute()) {

            $sql = \Database\getConnection()
                ->prepare("DELETE FROM Account_Edit WHERE token = ?");
            $sql->bind_param("s", $token);

            if ($sql->execute()) {

                $_SESSION['Success']['Type'] = "emailSet";
                $_SESSION['Success']['Message'] = "Email Changed";
                $_SESSION['User']['Email'] = $email;

                header("Location: " . $_SERVER['HTTP_REFERER']);

            }

        }

    }

    $sql->close();

} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}