<?php

if (\Functions\checkSet($_POST['token'], $_POST['value'])) {

    $token = $_POST['token'];
    $username = $_POST['value'];
    $user_ID = $_SESSION['User']['ID'];

    if(preg_match("/^[a-zA-Z0-9\_\-]{4,20}$/", $username)) {
        echo json_encode(array(
            'success' => false,
            'message' => "Invalid username, a-z, 0-9, _ and - only!"
        ));
        die();
    }

    if (!$sql = \Database\getConnection()
        ->prepare("SELECT * FROM Account_Edit WHERE token = ? AND user_ID = ? AND type = 'username'")) {

        echo json_encode(array(
            'success' => false,
            'message' => "Josh dun goofed... Shout at him!"
        ));
        die();
    }

    $sql->bind_param("si", $token, $user_ID);
    $sql->execute();
    $sql->store_result();
    $test = $sql->num_rows;

    if ($test == 1) {

        if(!\Database\Account\getUserByUsername($username)) {

            $sql = \Database\getConnection()
                ->prepare("UPDATE Users SET username = ? WHERE ID = ?");
            $sql->bind_param("si", $username, $user_ID);

            if ($sql->execute()) {

                $sql = \Database\getConnection()
                    ->prepare("DELETE FROM Account_Edit WHERE token = ?");
                $sql->bind_param("s", $token);

                if ($sql->execute()) {

                    echo json_encode(array(
                        'success' => true,
                        'message' => "Username changed!"
                    ));
                    header("Location: " . $_SERVER['HTTP_REFERER']);
                    die();

                }

            }

        } else {
            echo json_encode(array(
                'success' => false,
                'message' => "Username taken!"
            ));
            die();
        }

    } else {
        echo json_encode(array(
            'success' => false,
            'message' => "Verification token not found!"
        ));
        die();
    }

    $sql->close();

} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}