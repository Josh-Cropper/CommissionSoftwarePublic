<?php

if (isset($_REQUEST['passwordChangeVerification'])) {
    $token = $_REQUEST['passwordChangeVerification'];
    $sql = \Database\getConnection()
        ->prepare("SELECT user_ID, type FROM Account_Edit WHERE token = ?");
    $sql->bind_param("s", $token);
    $sql->execute();
    $sql->store_result();
    $sql->bind_result($user_ID, $type);
    $sql->fetch();
    if ($user_ID == $_SESSION['User']['ID'] && $type == "password") {
        echo "true";
    } else {
        echo "false\n[ $user_ID : " . $_SESSION['User']['ID'] . " ]\n[ $token : ";
    }
    $sql->close();
} else {
    header("Location: " . $_SERVER['HTTP_REFERER']);
}