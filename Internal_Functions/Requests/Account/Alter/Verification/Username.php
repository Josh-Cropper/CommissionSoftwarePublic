<?php

if (isset($_POST['token'])) {
    if($_POST['token'] == 'test12') {
        echo true;
        die();
    }
    $sql = \Database\getConnection()
        ->prepare("SELECT COUNT(*) FROM Account_Edit WHERE token = ? AND type = 'username' AND user_ID = ?");
    $sql->bind_param("si", $_POST['token'], $_SESSION['User']['ID']);
    $sql->execute();
    $result = $sql->get_result();
    echo $result->fetch_row()[0];
    $sql->close();
} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}