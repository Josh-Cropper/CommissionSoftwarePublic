<?php

if (isset($_POST['passwordChange'])) {

    $token = $_POST['vCode'];
    $password = Functions\encryptString($_POST['newPassword']);
    $user_ID = $_SESSION['User']['ID'];

    if (!$sql = \Database\getConnection()
        ->prepare("SELECT * FROM Account_Edit WHERE token = ? AND user_ID = ? AND type = 'password'")) {

        print "Failed Prepare";

    }

    $sql->bind_param("si", $token, $user_ID);
    $sql->execute();
    $sql->store_result();
    $test = $sql->num_rows;

    if ($test == 1) {

        $sql = \Database\getConnection()
            ->prepare("UPDATE Users SET password = ? WHERE ID = ?");
        $sql->bind_param("si", $password, $user_ID);
        if ($sql->execute()) {

            $sql = \Database\getConnection()
                ->prepare("DELETE FROM Account_Edit WHERE token = ?");
            $sql->bind_param("s", $token);

            if ($sql->execute()) {

                $_SESSION['Success']['Type'] = "passwordSet";
                $_SESSION['Success']['Message'] = "Password Changed";

                header("Location: " . \Settings::_i()->getRoot() . "/My_Account");

            }

        }

    }

    $sql->close();

}

    header("Location: " . $_SERVER['HTTP_REFERER']);