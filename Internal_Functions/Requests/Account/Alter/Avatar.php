<?php

if (isset($_POST['avatarSubmit'])) {

    $tmpName = $_FILES['image']['tmp_name'];
    $fp = fopen($tmpName, 'r');
    $data = fread($fp, filesize($tmpName));
    fclose($fp);

    Database\Account_Edit\changeAvatar($data, $_POST['userID']);

} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}