<?php

$username = $_POST['username'];
$email = $_POST['email'];

if (Database\Account_Creation\check_Email($email)) {

    $_SESSION['Error']['Type'] = "create_account";
    $_SESSION['Error']['Message'] = "Token Already Exists For Email";
    header("Location: " . $_SERVER['HTTP_REFERER']);
    die();

}

$value = Functions\randomString(10);

while (Database\Account_Creation\checkValue($value)) {

    $value = Functions\randomString(10);

}

$test = Database\Account_Creation\create_Account($value, $username, $email);

if ($test[0]) {

    $subject = "OvOcommissions Account Activation";

    $message = \Settings::_i()->emails->validate;
    $message = str_replace("\$token", $value, $message);

    Functions\sendEmail($_POST['email'], $subject, $message);

} else {

    $_SESSION['Error']['Type'] = "create_account";
    $_SESSION['Error']['Message'] = $test[1];

}
header("Location: " . $_SERVER['HTTP_REFERER']);