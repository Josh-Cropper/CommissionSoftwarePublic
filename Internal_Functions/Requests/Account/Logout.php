<?php

unset($_SESSION['User']);

session_destroy();

echo json_encode([
    'response' =>
        'window.location.replace("'.\Settings::_i()->getRoot().'/'
        .\PageBuilder::_i()->getMainPage().'")']);

//header("Location: " . \Settings::_i()->getRoot() . "/" .
//    PageBuilder::_i()->getMainPage());