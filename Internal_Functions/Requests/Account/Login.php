<?php
if(!\Database\Security\checkIPBan($_SERVER['REMOTE_ADDR'])) {

    $username = $_POST['username'];

    $password = \Functions\encryptString($_POST['password']);

    $username = stripslashes($username);
    $username = \Database\getConnection()->real_escape_string($username);

    $loginAttempt = \Database\login($username, $password);

    if ($loginAttempt[0]) {

        $_SESSION['User'] = $loginAttempt[1];

        \Database\log($_SERVER['REMOTE_ADDR'], $_SESSION['User']['ID'], 'login', json_encode([
            'success' => true,
            'phpSessionID' => session_id()
        ]));

        echo json_encode(['response' => 'location.reload()']);

    } else {

        $_SESSION['Error']['Type'] = $loginAttempt[1];
        $_SESSION['Error']['Message'] = $loginAttempt[2];

        \Database\log($_SERVER['REMOTE_ADDR'], 0, 'login', json_encode([
            'success' => false,
            'phpSessionID' => session_id()
        ]));

        if (\Database\Security\checkFailedLogins($_SERVER['REMOTE_ADDR'], 10) > 9) {
            \Database\Security\addIPBan($_SERVER['REMOTE_ADDR'], 20);
        }

    }

} else {
    \Database\log($_SERVER['REMOTE_ADDR'], 0, 'login', json_encode([
        'success' => false,
        'phpSessionID' => session_id(),
        'reason' => "IP Ban"
    ]));
}

//header("Location: " . $_SERVER['HTTP_REFERER']);