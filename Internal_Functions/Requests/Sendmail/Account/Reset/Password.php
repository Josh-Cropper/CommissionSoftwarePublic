<?php

if (isset($_POST['passwordResetEmail'])) {
    if(!isset($_POST['username'])) {
        echo json_encode([false, "Please provide a username."]);
        die();
    }

    \Database\Account_Edit\passwordResetEmail($_POST['username']);

} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}