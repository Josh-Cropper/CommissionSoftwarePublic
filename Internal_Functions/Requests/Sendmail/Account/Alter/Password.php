<?php

if (isset($_SESSION['User'])) {

    $subject = "OvOcommissions Password Change";

    $token = Functions\randomString(6);
    $user_ID = $_SESSION['User']['ID'];

    $sql = \Database\getConnection()->prepare("INSERT INTO Account_Edit (token, user_ID) VALUES (?,?)");

    if (!$sql->bind_param("si", $token, $user_ID)) {
        echo "Param binding failed!\nUser ID: $user_ID, Token: $token\n";
    }

    if ($sql->execute()) {
        $message = str_replace("\$Context", "Password", str_replace("\$context", "password", str_replace("\$token", $token, \Settings::_i()->emails->accountChange)));

        Functions\sendEmail($_SESSION['User']['Email'], $subject, $message);
        echo "true";

    } else {

        echo "false ", $sql->error;

    }

    $sql->close();

} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}