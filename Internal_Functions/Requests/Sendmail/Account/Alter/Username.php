<?php

if (isset($_SESSION['User'])) {

    $subject = "OvOcommissions Username Change";

    $token = Functions\randomString(6);
    $user_ID = $_SESSION['User']['ID'];

    $sql = \Database\getConnection()->prepare("INSERT INTO Account_Edit (token, type, user_ID) VALUES (?,'username',?)");

    if (!$sql->bind_param("si", $token, $user_ID)) {
        echo json_encode(array(
            'success' => false,
            'message' => "Failed to generate code!"
        ));
        $sql->close();
        die();
    }

    if ($sql->execute()) {
        $message = str_replace("\$Context", "Username", str_replace("\$context", "username", str_replace("\$token", $token, \Settings::_i()->emails->accountChange)));

        if(Functions\sendEmail($_SESSION['User']['Email'], $subject, $message)) {
            \Database\log($_SERVER['REMOTE_ADDR'], $user_ID, 'mail', json_encode([
                'success'       => true,
                'mailContext'   => 'Username Change',
                'body'          => $message,
                'to'            => $_SESSION['User']['Email']
            ]));
            echo json_encode(array(
                'success' => true,
                'message' => "Username change email sent!"
            ));
        } else {
            \Database\log($_SERVER['REMOTE_ADDR'], $user_ID, 'mail', json_encode([
                'success'       => false,
                'mailContext'   => 'Username Change',
                'body'          => $message,
                'to'            => $_SESSION['User']['Email']
            ]));
            echo json_encode(array(
                'success' => false,
                'message' => "Failed to send email, contact an administrator!"
            ));
        }

    } else {

        echo json_encode(array(
            'success' => false,
            'message' => "Code already exists!"
        ));

    }

} else {

    header("Location: " . $_SERVER['HTTP_REFERER']);

}