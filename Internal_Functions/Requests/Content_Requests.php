<?php
include_once '../Includes.php';

if(isset($_POST['commission_id']) && isset($_POST['id']) && \Functions\sessionCheck()) {

    echo json_encode(\Database\Images\fetch_commission_images((int)$_POST['commission_id'], false, (int)$_POST['id'], true, 90, 0));

} elseif (isset($_POST['build_element'])) {
    if(isset($_POST['build_element_args'])) {
        $args = $_POST['build_element_args'];
        PageBuilder::_i()->buildElement($_POST['build_element'], ...$args);
    } else {
        PageBuilder::_i()->buildElement($_POST['build_element']);
    }
} elseif (isset($_POST['getGalleryImages'])
    && $_POST['getGalleryImages'] == "true"
    && isset($_POST['galleryPage'])) {

    ( $images = \Database\Images\fetchGalleryImages((int)$_POST['galleryPage'], false) );

    $returnImages = array();

    foreach ($images as $image) {
        $returnImages[] = array(
            'name' => $image['name'],
            'version' => $image['version'],
            'username' => $image['username'],
            'data' => \Image_Handler::_i()->displayImage($image['ID'], 110, 180, true)
        );
    }

    echo json_encode($returnImages);
}