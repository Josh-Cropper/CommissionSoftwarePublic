<?php declare(strict_types=1);

namespace Functions {

    function randomString(int $length): string
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPGRSTUVWXYZ1234567890_";
        $string = '';
        $max = mb_strlen($chars, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $string .= $chars[mt_rand(0, $max)];
        }
        return $string;
    }

    function sendEmail(string $to, string $subject, string $message): bool
    {
        $to = '<' . $to . '>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type:text/html;charset=UTF-8';
        $headers[] = 'Return-Path: no-reply@roll6.com \r\n';
        $headers[] = 'Organization: Roll6 \r\n';
        $headers[] = 'X-Mailer: PHP/' . phpversion();

        $headers[] = 'From: Roll6.com <no-reply@roll6.com>';

        return mail($to, $subject, $message, implode("\r\n", $headers), "-f no-reply@roll6.com");
    }

    function checkAccess(string $ranks, int $userRank): bool
    {
        $ranksArray = explode(',', $ranks);
        $modifier = array_shift($ranksArray);
        foreach ($ranksArray as $val) {
            if ($modifier == "none") {
                $intVal = intval($val);
                if ($userRank == $intVal) {
                    return true;
                }
            } else if ($modifier == "all") {
                $intVal = abs(intval($val));
                if (abs($userRank) == $intVal) {
                    return false;
                }
            }
        }
        if ($modifier == "none") {
            return false;
        } else if ($modifier == "all") {
            return true;
        }
    }

    function encryptString(string $string): string
    {
        $password = hash("sha256", hash("sha256", \Settings::_i()->salt . $string)
            . \Settings::_i()->salt);
        $password = stripslashes($password);
        $password = \Database\getConnection()->real_escape_string($password);

        $password = str_split($password, strlen($password) / 2);
        return $password[1] . $password[0];
    }

    function sessionCheck(): bool
    {
        if (isset($_SESSION['User'])) {
            return true;
        }
        return false;
    }

    function checkSet(bool $or = false, ...$vars): bool
    {
        foreach ($vars as $var) {
            if($or) {
                if(isset($var) && !empty($var)) {
                    return true;
                }
            } else {
                if (!isset($var) || empty($var)) {
                    return false;
                }
            }
        }
        return !$or;
    }
}