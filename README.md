# CommissionSoftware

Open source art commissioning store/management software.

### WIP
1. [ ] **Framework** Single line user validation forms. E.G: Password resets, username changes, etc.
1. [ ] **Framework** Action logging

### Planned
1. [ ] **Software** Move websocket based chat out of testing and into commission pages.
1. [ ] **Software** Database vs Filesystem image storage option.


### Done
1. [x] **Framework** Added bans, with automatic IP bans for excessive failed login attempts, to prevent brute-force attacks.
1. [x] Hide confidential data without .gitignore on the settings file so that I can push directly to public rather than private.
1. [x] A lot more stuff that I can't remember.


## Optional
1. APCu - highly reccomended