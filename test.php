<?php
$itemID = $_REQUEST['item'];

do {
    @mkdir(__DIR__."\cache");

    if ($file = fopen(__DIR__ . "\cache\.$itemID", "r")) {
        $json = stream_get_contents($file);
        fclose($file);

        $timestamp = json_decode($json, true);
        var_dump($timestamp);
        if (isset($timestamp['timestamp'])
            && (time() - $timestamp['timestamp']) < 180) {
            $data = $json;
            continue;
        }
    }

    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => "https://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json?item={$itemID}"
    ));
    $data = json_decode(curl_exec($ch), true);
    curl_close($ch);

    $data['timestamp'] = time();
    $data = json_encode($data);
    if ($file = fopen(__DIR__ . "\cache\.$itemID", "w")) {
        fwrite($file, $data);
        fclose($file);
    }

} while (false);

echo str_replace("\/","/",$data);