<?php
declare (strict_types = 1);

///Includes///
include_once '../Internal_Functions/Includes.php';
///Includes///

if(!isset($_SESSION['User']) || $_SESSION['User']['Rank'] != 999) {
    header("Location: ".\Settings\getRoot()."/".PageBuilder::_i()->getMainPage());
}

$page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "";

$page = trim($page, "/");

//echo $page;

if ($page == "") {
} else if (in_array($page, PageBuilder::_i()->getPages()->xpath("/PAGES/page/title"))) {
    PageBuilder::_i()->buildPage($page);
} else {
    $pageArray = explode("/", $page);
    if (PageBuilder::_i()->pageExists($pageArray[0])) {
        if (PageBuilder::_i()->isSubHandler($pageArray[0])) {
            PageBuilder::_i()->buildPage($pageArray[0]);
        } else {
            PageBuilder::_i()->buildPage("404");
        }
    } else {
        PageBuilder::_i()->buildPage("404");
    }
}

class INDEX
{

    public static function getPage()
    {
        global $page;
        if ($page != "") {
            return $page;
        }
        return PageBuilder::_i()->getMainPage();
    }
}
?>