<?php
declare (strict_types = 1);

///Includes///
require_once 'Internal_Functions/Includes.php';
///Includes///
$page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "";

$page = trim($page, "/");

if ($page == "") {
    PageBuilder::_i()
        ->buildPage(stripslashes(PageBuilder::_i()->getMainPage()));
} else if (in_array($page, PageBuilder::_i()->getPages()->xpath("/PAGES/page/title"))) {
    PageBuilder::_i()->buildPage($page);
} else {
    $pageArray = explode("/", $page);
    if (PageBuilder::_i()->pageExists($pageArray[0])) {
        if (PageBuilder::_i()->isSubHandler($pageArray[0])) {
            PageBuilder::_i()->buildPage($pageArray[0]);
        } else {
            PageBuilder::_i()->buildPage("404");
        }
    } else {
        PageBuilder::_i()->buildPage("404");
    }
}

class INDEX
{

    public static function getPage()
    {
        global $page;
        if ($page != "") {
            return $page;
        }
        return PageBuilder::_i()->getMainPage();
    }
}
